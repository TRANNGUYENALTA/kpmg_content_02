var zoomIn = true;
var auditColor = '#0191d8';
var taxColor = '#bc1f4a';
var advisoryColor = '#eaaa00';
var centralsrvColor = '#c3007b';
var datas = '';
var isToggle = false;

var isImageFeed = false;
var isImagePromo = false;
var isImagePromTemplate_02 = false;
var isImagePromTemplate_03 = false;
var isVideoPromTemplate_03 = false;
var isImageLogoTemplate_02 = false;
var isImageLogoPromTemplate_02 = false;
var isImageFeedInfo = false;
var base64StringTopInJs = '';
var base64StringBottomInJs = '';
// gradient image
var gradientImageTop = '';
var gradientImageBottom = '';
$(document).ready(function () {

  // delete image error

  // refresh page
  $(document).on('click', '#resfeshDashBoard', function () {
    $('#icon-search').click();
  });
  // active gradient

  $(document).on('click', '.gradient_item_bottom', function () {
    $('.gradient_item_bottom').removeClass('active');
    $(this).addClass('active');
  });

  $(document).on('click', '.gradient_item_top', function () {
    $('.gradient_item_top').removeClass('active');
    $(this).addClass('active');
  });

  $(document).on('click', '.gradient_template-02', function () {
    $('.gradient_template-02').removeClass('active');
    $(this).addClass('active');
  });
  $(document).on('click', '.gradient_item_template_03', function () {
    $('.gradient_item_template_03').removeClass('active');
    $(this).addClass('active');
  });


  // end active gradient
  $('div[contenteditable]').keydown(function (e) {
    // trap the return key being pressed
    if (e.keyCode === 13) {
      // insert 2 br tags (if only one br tag is inserted the cursor won't go to the next line)
      document.execCommand('insertHTML', false, '<br><br>');
      // prevent the default behaviour of return key pressed
      return false;
    }
  });

  $('#multi-select').dropdown();
  //click icon search
  // Dragable
  $('#map-office').draggable();
  $('.avatar-office').draggable();
  // click map office zoomin
  ;
  //set width progress bar
  setWidthProgressBar();


  // set color background menu item
  // clickItem()
});

function setNavLeftIcon() {
  setIconActiveNavLink();
  // Set Title Page
  $(document).on('click', '.nav-item', function () {
    var tt = $(this).children('.nav-link').data('title');
    $('#titlePage').text(tt);
  });
  $('.list-menu .nav-item').on('click', function () {
    $('.list-menu .nav-item').removeClass('active');
    $(this).addClass('active');
    setIconNormalNavLink();
    setIconActiveNavLink();
    setMouseoverMenuNav();
    setMouseOutMenuNav();
    var srcActive = $(this).children('.nav-link').children('.icon-nav').data('link-active');
    $(this).children('.nav-link').children('.icon-nav').attr('src', srcActive);
  })
  setMouseoverMenuNav();
  setMouseOutMenuNav();
}

function clickItem() {
  $(document).on('click', '.menu .item', function () {
    set();
  })
}

function setMouseoverMenuNav() {
  $('.list-menu .nav-item').not('.active').children('.nav-link').mouseover(function () {
    var src = $(this).children('.icon-nav').data('link-active');
    $(this).children('.icon-nav').attr('src', src);
    $('.list-menu .nav-item .icon-nav').css({'width': '25px!important', 'height': 'auto!important'});
    setIconActiveNavLink();
  });
}

function setMouseOutMenuNav() {
  $('.list-menu .nav-item').not('.active').children('.nav-link').mouseout(function () {
    var src = $(this).children('.icon-nav').data('link-normal');
    $(this).children('.icon-nav').attr('src', src);
    $('.list-menu .nav-item .icon-nav').css({'width': '25px!important', 'height': 'auto!important'});
    setIconActiveNavLink();
  });
}

function setIconActiveNavLink() {
  $('.list-menu .nav-item.active').each(function () {
    var src = $(this).children('.nav-link').children('.icon-nav').data('link-active');
    $(this).children('.nav-link').children('.icon-nav').attr('src', src);
  })
}

function setIconNormalNavLink() {
  $('.list-menu .nav-item').each(function () {
    var src = $(this).children('.nav-link').children('.icon-nav').data('link-normal');
    $(this).children('.nav-link').children('.icon-nav').attr('src', src);
  })
}

// set icon profile color
function setColorProfile(obj, css) {
  $(obj).each(function () {
    var type = $(this).data('profile');
    switch (type) {
      case 'audit': {
        $(this).css(css, auditColor);
        break;
      }
      case 'tax': {
        $(this).css(css, taxColor);
        break;
      }
      case 'advisory': {
        $(this).css(css, advisoryColor);
        break;
      }
      case 'centralsrv': {
        $(this).css(css, centralsrvColor);
        break;
      }
    }
  });
}


function set() {
  setTimeout(function () {
    setBackgroundProfile();
  }, 50);
}

// set background profile
function setBackgroundProfile() {
  $('.label').each(function () {
    for (i = 0; i < datas.length; i++) {
      if ($(this).data('value') === datas[i].key) {
        $(this).css('background-color', datas[i].value)
      }
    }
  });
}

function closeModal() {
  $('.close').click();
}

// map
function mapAction() {
  $('#map-office').draggable();
  $('.avatar-office').draggable();
}

function setWidthProgressBar() {
  var father = $('.each-favorite').each(function () {
    var perCent = $(this).children('.progress-bar').data('percent');
    $(this).children('.progress-bar').css('width', perCent);
    $(this).children('.number-percent').text(' ' + perCent + ' %');
  });
}

function setHeightElement(ele) {
  // set height for tab-content
  var height = $(window).height() - $(ele).offset().top - 20;
  $(ele).css('height', height);
}

function zoomin() {

  // zoomIn = true;
  // var myImg = $("#myCanvas");
  // var curWidth = myImg.width();
  // var curHeight = myImg.height();
  // if (curWidth === 2500) return false;
  // else {
  //   let width = curWidth + curWidth * 0.2;
  //   let height = width * curHeight / curWidth;
  //   myImg.css("width", width);
  //   myImg.css("height", height);
  // }


}

function zoomout() {
  zoomIn = false;
  var myImg = $("#myCanvas");
  var curWidth = myImg.width();
  var curHeight = myImg.height();
  if (curWidth === 100) return false;
  else {
    let width = curWidth - curWidth * 0.2;
    let height = width * curHeight / curWidth;
    myImg.css("width", width);
    myImg.css("height", height);
  }
}

//side bar
function openNav(ele) {
  $(ele).css('right', '0');
}

function closeNav(ele) {
  $(ele).css('right', '-420px');
}

function openChatBox(ele) {
  $(ele).css('right', '45px');
}

function closeChatBox(ele) {
  $(ele).css('right', '-410px');
}

//close form create
function closeFormCreate() {
  $('#close-create-rule').click();
}

function closeModalMail() {
  $('#modalbox-email-header-current .close').click();

}

function toggleMenu() {
  isToggle = !isToggle;
  if (isToggle) {
    $('#menuLeft').addClass('toggle-menu');
    $('#content-right').addClass('toggle-menu');
    $('#menuLeft_responsive').addClass('toggle-menu');
  } else {
    $('#menuLeft').removeClass('toggle-menu');
    $('#content-right').removeClass('toggle-menu');
    $('#menuLeft_responsive').removeClass('toggle-menu');
  }
}

function checkImageFeed(obj) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if (this.width == 133 && this.height == 90) {
          isImageFeed = true;
        } else {
          isImageFeed = false;
        }

      };
      img.src = _URL.createObjectURL(file);
    } else {
      isImageFeed = true;
    }
  });
}

function checkImagePromo(obj) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if (this.width == 600 && this.height == 590) {
          isImagePromo = true;
        } else {
          isImagePromo = false;
        }

      };
      img.src = _URL.createObjectURL(file);
    } else {
      isImagePromo = true;
    }
  });
}

function checkImagePromoTemplate02(obj) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if (this.width == 600 && this.height == 1080) {
          isImagePromTemplate_02 = true;
        } else {
          isImagePromTemplate_02 = false;
        }

      };
      img.src = _URL.createObjectURL(file);
    } else {
      isImagePromTemplate_02 = true;
    }
  });
}

function checkImagePromoTemplate03(obj) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if (this.width == 600 && this.height == 765) {
          isImagePromTemplate_03 = true;
        } else {
          isImagePromTemplate_03 = false;
        }

      };
      img.src = _URL.createObjectURL(file);
    } else {
      isImagePromTemplate_03 = true;
    }
  });
}

function checkImageLogoTemplate02(obj) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if (this.width == 160 && this.height == 50) {
          isImageLogoPromTemplate_02 = true;
        } else {
          isImageLogoPromTemplate_02 = false;
        }

      };
      img.src = _URL.createObjectURL(file);
    } else {
      isImageLogoPromTemplate_02 = true;
    }
  });
}


// set canvas to launch
function setCanVasToLaunch(pos, from, to) {
  var canvas = document.getElementById('templateCanvas');
  var ctx = canvas.getContext('2d');
  // Create gradient
  var grd = ctx.createLinearGradient(0,0,600,540);
  grd.addColorStop(0, from);
  grd.addColorStop(1, to);
// Fill with gradient
  ctx.fillStyle = grd;
  ctx.fillRect(0,0,600,540);
  // text
  // ctx.font="40px KPMG";
  // ctx.fillText("Welcome to KPMG",10,50);

  var image_me = canvas.toDataURL('image/png');
  if (pos == 1) {
    gradientImageTop = image_me;
  } else {
    gradientImageBottom = image_me;
  }
}
function setTitleCanvasLaunch(title){
  var canvas = document.getElementById('templateCanvas');
  var ctx = canvas.getContext('2d');
  ctx.font="20px KPMG";
  ctx.fillText(title,10,50);
}

// arrImg.push($('.item').length);
function readURLFilePromo(input, isTop) {
  setTimeout(function () {
    if (input.files && input.files[0]) {
      var reader = new FileReader()
      reader.onload = function (e) {
        if (isImagePromo) {
          if (isTop == 1) {
            $('.img_template_top').attr('src', e.target.result);
            $('.img_template_top').css('display','inline-block');
            $('#lable-file-promo').text(input.files[0].name);
            $('#lable-file-promo').attr('title', input.files[0].name);
          } else {
            $('.img_template_bottom').attr('src', e.target.result);
            $('#lable-file-promo-bottom').text(input.files[0].name);
            $('#lable-file-promo-bottom').attr('title', input.files[0].name);
          }
        } else {
          if (isTop == 1) {
            $('.img_template_top').attr('src', '');
          } else {
            $('.img_template_bottom').attr('src', '');
          }
        }
      };
      reader.readAsDataURL(input.files[0]);
      formData.append('file[]', input.files[0]);
    }
    if (input.files.length === 0) {
      if (isTop == 1) {
        $('.img_template_top').attr('src', '');
        $('#lable-file-promo').text("");
      } else {
        $('.img_template_bottom').attr('src', '');
        $('#lable-file-promo-bottom').text("");
      }
    }
  }, 20)
}

// template 02
function readURLFilePromoTemplate02(input) {
  setTimeout(function () {
    if (input.files && input.files[0]) {
      var reader = new FileReader()
      reader.onload = function (e) {
        if (isImagePromTemplate_02) {
          $('.img_template_02').attr('src', e.target.result);
          $('.img_template_02').css('display', 'inline-block');
          $('#lable-file-promo_02').text(input.files[0].name);
          $('#lable-file-promo_02').attr('title', input.files[0].name);
        } else {
          $('.img_template_02').attr('src', '');
          $('#lable-file-promo_02').text("Upload Image");
          $('#lable-file-promo_02').attr('title', "Upload Image");
        }
      };
      reader.readAsDataURL(input.files[0]);
      formData.append('file[]', input.files[0]);
    }
    if (input.files.length === 0) {
      $('.img_template_02').attr('src', '');
      $('#lable-file-promo_02').text("Upload Image");
      $('#lable-file-promo_02').attr('title', "Upload Image");
    }
  }, 20)
}

function readURLFileLogoPromoTemplate02(input) {
  setTimeout(function () {
    if (input.files && input.files[0]) {
      var reader = new FileReader()
      reader.onload = function (e) {
        if (isImageLogoPromTemplate_02) {
          $('.img_logo_template_02').attr('src', e.target.result);
          $('.img_logo_template_02').css('display', 'inline-block');
          $('#lable-file-logo-promo_02').text(input.files[0].name);
          $('#lable-file-logo-promo_02').attr('title', input.files[0].name);
        } else {
          $('.img_logo_template_02').attr('src', '');
          $('#lable-file-logo-promo_02').text("Upload Image");
          $('#lable-file-logo-promo_02').attr('title', "");
        }
      };
      reader.readAsDataURL(input.files[0]);
      formData.append('file[]', input.files[0]);
    }
    if (input.files.length === 0) {
      $('.img_logo_template_02').attr('src', '');
      $('#lable-file-logo-promo_02').text("Upload Image");
      $('#lable-file-logo-promo_02').attr('title', "");
    }
  }, 20)
}

// template 03
function readURLFilePromoTemplate03(input) {
  setTimeout(function () {
    if (input.files && input.files[0]) {
      var reader = new FileReader()
      reader.onload = function (e) {
        if (isImagePromTemplate_03) {
          $('.img_template_03').attr('src', e.target.result);
          $('.img_template_03').css('display', 'inline-block');
          $('#lable-file-promo_03').text(input.files[0].name);
          $('#lable-file-promo_03').attr('title', input.files[0].name);
        } else {
          $('.img_template_03').attr('src', '');
          $('#lable-file-promo_03').text("Upload Image");
          $('#lable-file-promo_03').attr('title', "");
        }
      };
      reader.readAsDataURL(input.files[0]);
      formData.append('file[]', input.files[0]);
    }
    if (input.files.length === 0) {
      $('.img_template_03').attr('src', '');
      $('#lable-file-promo_03').text("Upload Image");
      $('#lable-file-promo_03').attr('title', "");
    }
  }, 20)
}

// Read file video in template 03
function readURLFileVideoPromoTemplate03(input) {
  var URL = window.URL || window.webkitURL;
  var file = input.files[0];
  var videoNode = document.querySelector('.video_template03');
  var videoNodeSize = document.querySelector('#video_size');
  var videoNodeContent03 = document.querySelector('.video_template03_content_03');
  setTimeout(function () {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(input.files[0]);
      reader.onload = function (e) {
        var fileURL = URL.createObjectURL(file);
        videoNodeSize.src = fileURL;
        if (isVideoPromTemplate_03) {
          videoNode.src = fileURL;
          videoNode.style.zIndex = '1000';
          videoNodeContent03.src = fileURL;
          videoNodeContent03.style.zIndex = '1000';

          // $('.img_template_03').attr('src', e.target.result);
          $('#lable-file-video-promo_03').text(input.files[0].name);
          $('#lable-file-video-promo_03').attr('title', fileURL);
        } else {
          videoNode.src = '';
          videoNodeSize.src = '';
          videoNode.style.zIndex = '0';
          videoNodeContent03.src = '';
          videoNodeContent03.style.zIndex = '0';
          $('#lable-file-video-promo_03').text("Upload video");
          $('#lable-file-video-promo_03').attr('title', "");
        }
      };
    }
    if (input.files.length === 0) {
      videoNode.src = '';
      videoNodeSize.src = '';
      videoNode.style.zIndex = '0';
      videoNodeContent03.src = '';
      videoNodeContent03.style.zIndex = '0';
      $('#lable-file-video-promo_03').text("Upload video");
      $('#lable-file-video-promo_03').attr('title', "");
    }
  }, 20)
}

function checkVideoSizeTemplate(obj, w, h) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var _videoSize = $('#video_size');
    if ((file = this.files[0])) {
      var width = _videoSize.width();
      var height = _videoSize.height();
      if (this.width == width && this.height == height) {
        isVideoPromTemplate_03 = true;
      } else {
        isVideoPromTemplate_03 = true;
      }
    } else {
      isVideoPromTemplate_03 = false;
    }
  });
}

// convert image to base 64
function convertImageTopBase64(input) {
  base64StringInJs = null;
  if (input.files && input.files[0]) {
    var f = input.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function (theFile) {
      return function (e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        base64StringTopInJs = 'data:image/png;base64,' + window.btoa(binaryData);
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  } else {
    base64StringTopInJs = null;
  }
}

// convert image to base 64
function convertImageBottomoBase64(input) {
  base64StringBottomInJs = null;
  if (input.files && input.files[0]) {
    var f = input.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function (theFile) {
      return function (e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        base64StringBottomInJs = 'data:image/png;base64,' + window.btoa(binaryData);
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  } else {
    base64StringBottomInJs = null;
  }
}

function convertVideoBase64(input) {
  base64StringBottomInJs = null;
  if (input.files && input.files[0]) {
    var f = input.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function (theFile) {
      return function (e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        base64StringBottomInJs = 'data:video/mp4;base64,' + window.btoa(binaryData);
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  } else {
    base64StringBottomInJs = null;
  }
}

function checkImageFeedInfo(obj) {
  var _URL = window.URL || window.webkitURL;
  $(document).on('change', obj, function () {
    var file, img;
    if ((file = this.files[0])) {
      img = new Image();
      img.onload = function () {
        if (this.width == 132 && this.height == 111) {
          isImageFeedInfo = true;
        } else {
          isImageFeedInfo = false;
        }
      };
      img.src = _URL.createObjectURL(file);
    } else {
      isImageFeedInfo = true;
    }
  });
}

function css() {
  $('.recurrence').css('color', '#c3bfbf').css('background-color', '#dedbdb');
}

function ShowDetail(id) {

  setTimeout(function () {
    $("#ShowDetail").trigger('click')
  }, 1)
  $("#hidId").val(id)

}
