var date_temp = '';
var date_tampchart1 = '';
var month1_temp = 0;
var month2_temp = 0;

EXPORT_WIDTH = 1000;

function show_dp() {
  $("#datepicker").datepicker('show');
}

function show_dp2() {
  $('#datepicker2').MonthPicker({
    Button: false,
    MonthFormat: 'MM'
  });

}

function show_dp3() {
  $("#datepicker3").datepicker('show');
}


function drawChart1(dl, ip) {
  $('#overview-tab').on('click', function () {
    $('.carousel').on('slid.bs.carousel', checkitem);
    checkitem();
  })

  function checkitem() {
    var $this = $('.carousel');
    if ($('.carousel-inner .carousel-item:first').hasClass('active')) {
      $('.left.carousel-control').hide();
      document.getElementById('title-home').innerHTML = "Overview: Day";
      // console.log($this.data);
    } else if ($('.carousel-inner .carousel-item:last').hasClass('active')) {
      document.getElementById('title-home').innerHTML = "Overview: Zone";
      $('.right.carousel-control').hide();
    } else {
      document.getElementById('title-home').innerHTML = "Overview: Month";
      $('.carousel-control').show();
    }
  }

  $('.function-apend').empty();
  $('.function-apend1').empty();
  $('.function-apend2').empty();
  var today = new Date();
  var today_chart1 = new Date();
  var datedownload = new Date();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();
  var yy = today.getYear() - 100;
  var dd_1 = today_chart1.getDate();
  var mm_1 = today_chart1.getMonth() + 1; //January is 0!
  if (dd_1 < 10) {
    dd_1 = '0' + dd_1
  }
  if (mm_1 < 10) {
    mm_1 = '0' + mm_1
  }
  today_chart1 = mm_1 + '/' + dd_1 + '/' + yyyy;
  datedownload = yyyy+''+mm_1+''+dd_1;
  var txt_month;
  var txt_month1;
  if (month1_temp) {
    mm = month1_temp;
  }
  switch (mm) {
    case 1:
      txt_month = "January";
      break;
    case 2:
      txt_month = "February";
      break;
    case 3:
      txt_month = "March";
      break;
    case 4:
      txt_month = "April";
      break;
    case 5:
      txt_month = "May";
      break;
    case 6:
      txt_month = "June";
      break;
    case 7:
      txt_month = "July";
      break;
    case 8:
      txt_month = "August";
      break;
    case 9:
      txt_month = "September";
      break;
    case 10:
      txt_month = "October";
      break;
    case 11:
      txt_month = "November";
      break;
    case 12:
      txt_month = "December";
      break;
  }
  $('#datepicker2').val(txt_month);
  if (month2_temp) {
    mm = month2_temp;
  }
  switch (mm) {
    case 1:
      txt_month1 = "January";
      break;
    case 2:
      txt_month1 = "February";
      break;
    case 3:
      txt_month1 = "March";
      break;
    case 4:
      txt_month1 = "April";
      break;
    case 5:
      txt_month1 = "May";
      break;
    case 6:
      txt_month1 = "June";
      break;
    case 7:
      txt_month1 = "July";
      break;
    case 8:
      txt_month1 = "August";
      break;
    case 9:
      txt_month1 = "September";
      break;
    case 10:
      txt_month1 = "October";
      break;
    case 11:
      txt_month1 = "November";
      break;
    case 12:
      txt_month1 = "December";
      break;
  }
  $('#datepicker3').val(txt_month1);
  document.getElementById('sumNum3').innerHTML = txt_month1;
  // document.getElementById('sumNum4').innerHTML = txt_month1;
  $('.nav-item').click(function () {
    var tt = $(this).children('.nav-link').data('title');
    $('#titlePage').text(tt);
  });

  function save_chart(chart, filename) {
    var render_width = EXPORT_WIDTH;
    var render_height = render_width * chart.chartHeight / chart.chartWidth
    var svg = chart.getSVG({
      exporting: {
        sourceWidth: chart.chartWidth,
        sourceHeight: chart.chartHeight
      }
    });
    var canvas = document.createElement('canvas');
    canvas.height = render_height;
    canvas.width = render_width;
    var image = new Image;
    image.onload = function () {
      canvas.getContext('2d').drawImage(this, 0, 0, render_width, render_height);
      var data = canvas.toDataURL("image/png")
      download(data, filename + '.png');
    };
    image.src = 'data:image/svg+xml;base64,' + window.btoa(svg);
  }

  function download(data, filename) {
    var a = document.createElement('a');
    a.download = filename;
    a.href = data
    document.body.appendChild(a);
    a.click();
    a.remove();
  }

  $(document).on('click', '#abc', function () {
    save_chart($('#container').highcharts(), 'report_'+datedownload+'_clubhouse_daily');
  })
  $(document).on('click', '#abc2', function () {
    save_chart($('#container2').highcharts(), 'report_'+datedownload+'_clubhouse_monthly');
  })
  $(document).on('click', '#abc3', function () {
    save_chart($('#container3').highcharts(), 'report_'+datedownload+'_clubhouse_zone');
  })

  // CHART 1
  if (date_temp) {
    today_chart1 = date_temp;
    $('#datepicker').val(date_tampchart1);
  }
  else {
    $('#datepicker').val(today_chart1);
  }
  // var container = document.createElement('div');
  // var container2 = document.createElement('div');
  // var container3 = document.createElement('div');
  $('#datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
    autoclose: true,
  }).on("change", function () {
    var day = $(this).datepicker('getDate').getDate();
    var month = $(this).datepicker('getDate').getMonth() + 1;
    var year = $(this).datepicker('getDate').getFullYear();
    var date_chart1 = year + '-' + month + '-' + day;
    if (month < 10) {
      month = '0' + month
    }
    if (day < 10) {
      day = '0' + day
    }
    date_tampchart1 = month + '/' + day + '/' + year;
    date_temp = date_chart1;
    $.ajax({
      url: ip + '/api/statitic/chart1',
      type: 'POST',
      data: {
        "date": date_chart1,
        "function": dl
      },
      success: function (data) {
        console.log(data);
        $('.delete').trigger('click');
        Highcharts.chart('container', {
          chart: {
            type: 'spline',
            scrollablePlotArea: {
              minWidth: 500,
              scrollPositionX: 1
            }
          },
          title: {
            text: ''
          },
          xAxis: {
            type: 'datetime',
            lineColor: '#dbdbdb',
            lineWidth: 1,
            labels: {
              style: {
                fontWeight: 'bold',
              },
            },
          },
          yAxis: {
            lineColor: '#dbdbdb',
            lineWidth: 1,
            min: 0,
            title: {
              text: ' '
            }
          },
          tooltip: {
            shadow: false,
            backgroundColor: '#4a4a4a',
            borderColor: '#4a4a4a',
            borderRadius: 20,
            style: {
              color: '#ffffff',
            },
            useHTML: true,
            headerFormat: '<b>{point.key}<br></b>',
            formatter: function () {
              // console.log(data)
              var p = data.color;
              var arr = [];
              var arr1 = [];
              for (var key in p) {
                if (p.hasOwnProperty(key)) {
                  arr.push(key);
                  arr1.push(p[key]);
                }
              }
              var temp = -1;
              html = '<div class="mytooltip"> <div class="row">\n' +
                '<div class="col-7 d-flex flex-wrap align-self-center align-items-center" style="border-right:1px solid #ffffff;">\n';
              $.each(this.point.dt, function (k, v) {
                temp += 1;
                var colorpoint = '';
                if (k == arr[temp]) {
                  colorpoint = arr1[temp];
                }
                html += '<div class="col-6"><i class="fas fa-circle" style="color:' + colorpoint + '"></i>&nbsp;' + v + '&nbsp;&nbsp;</div>';
              })
              html += '</div>\n' +
                '<div class="col-5">\n' +
                '<b style="font-size: 22px;">' + this.y + '</b> <br>' +
                '<a style="font-size: 12px"> ' + Highcharts.dateFormat('@%I%p', this.x) + '</a>' +
                '</div>\n' +
                '</div>\n' +
                '</div>';
              return html;
            }
          },
          plotOptions: {
            spline: {
              lineWidth: 2,
              states: {
                hover: {
                  lineWidth: 3
                }
              },
              marker: {
                enabled: false
              },
              // pointStart: Date.UTC(2018, 9, 1, 8,0,0),
              pointStart: data.created,
              pointInterval: 3600000, // one hour
            }
          },
          series: [{
            color:'#d5708f',
            name: ' ',
            lineColor: '#d5708f',
            marker: {
              fillColor: '#d5708f', // dau tron
              lineWidth: 2,
              lineColor: '#d5708f'
            },
            data: data.data,
          }],
          navigation: {
            menuItemStyle: {
              fontSize: '10px'
            }
          },
          legend:{
            enabled: false,
            navigation:{
              enabled:false
            }
          }
        });
        // table footer
        $(document).ready(function () {
          $('.function-apend').empty();
          var sum = 0;
          var p = data.color;
          var arr = [];
          var arr1 = [];
          for (var key in p) {
            if (p.hasOwnProperty(key)) {
              arr.push(key);
              arr1.push(p[key]);
            }
          }
          var temp = -1;
          $.each(data.dulieu, function (i, l) {
            sum += l;
            temp += 1;
            console.log(temp)
            var colorpoint1 = '';
            if (i == arr[temp]) {
              colorpoint1 = arr1[temp];
            }
            $('.function-apend').append('<div class=" box_tam col-6 col-md-3">\n' +
              '<div align="center" class="chart--box box--1" style="background-color:' + colorpoint1 + '!important;">\n' +
              '<p class="text-white box--1--txt">' + i + '</p>\n' +
              '<p class="text-white box--1--num">' + l + '</p>\n' +
              '</div>\n' +
              '</div> ');
          });
          document.getElementById('sumNum').innerHTML = sum;
        });
      },
      error: function (a) {
        console.log(a)
      }
    });
  });
  $.ajax({
    url: ip + '/api/statitic/chart1',
    type: 'POST',
    data: {
      "date": today_chart1,
      "function": dl
    },
    success: function (data) {

      var sum = 0;
      Highcharts.chart('container', {
        chart: {
          renderTo:"container",
          type: 'spline',
          scrollablePlotArea: {
            minWidth: 500,
            scrollPositionX: 1
          }
        },
        title: {
          text: ''
        },
        xAxis: {
          type: 'datetime',
          lineColor: '#dbdbdb',
          lineWidth: 1,
          labels: {
            style: {
              fontWeight: 'bold',
            },
          },
        },
        yAxis: {
          lineColor: '#dbdbdb',
          lineWidth: 1,
          min: 0,
          title: {
            text: ' '
          }
        },
        tooltip: {
          shadow: false,
          backgroundColor: '#4a4a4a',
          borderColor: '#4a4a4a',
          borderRadius: 20,
          style: {
            color: '#ffffff',
          },
          useHTML: true,
          headerFormat: '<b>{point.key}<br></b>',
          formatter: function () {
            // console.log(data)
            var p = data.color;
            var arr = [];
            var arr1 = [];
            for (var key in p) {
              if (p.hasOwnProperty(key)) {
                arr.push(key);
                arr1.push(p[key]);
              }
            }
            var temp = -1;
            html = '<div class="mytooltip"> <div class="row">\n' +
              '<div class="col-7 d-flex flex-wrap align-self-center align-items-center" style="border-right:1px solid #ffffff;">\n';
            $.each(this.point.dt, function (k, v) {
              temp += 1;
              var colorpoint = '';
              if (k == arr[temp]) {
                colorpoint = arr1[temp];
              }
              html += '<div class="col-6"><i class="fas fa-circle" style="color:' + colorpoint + '"></i>&nbsp;' + v + '&nbsp;&nbsp;</div>';
            })
            html += '</div>\n' +
              '<div class="col-5">\n' +
              '<b style="font-size: 22px;">' + this.y + '</b> <br>' +
              '<a style="font-size: 12px"> ' + Highcharts.dateFormat('@%I%p', this.x) + '</a>' +
              '</div>\n' +
              '</div>\n' +
              '</div>';
            return html;
          }
        },
        plotOptions: {
          spline: {
            lineWidth: 2,
            states: {
              hover: {
                lineWidth: 3
              }
            },
            marker: {
              enabled: false
            },
            // pointStart: Date.UTC(2018, 9, 1, 8,0,0),
            pointStart: data.created,
            pointInterval: 3600000, // one hour
          }
        },
        series: [{
          color:'#d5708f',
          name: ' ',
          lineColor: '#d5708f',
          marker: {
            fillColor: '#d5708f', // dau tron
            lineWidth: 2,
            lineColor: '#d5708f'
          },
          data: data.data,
        }],
        navigation: {
          menuItemStyle: {
            fontSize: '10px'
          }
        },
        legend:{
          enabled: false,
          navigation:{
            enabled:false
          }
        }
      });
      // table footer
      $(document).ready(function () {
        var p = data.color;
        var arr = [];
        var arr1 = [];
        for (var key in p) {
          if (p.hasOwnProperty(key)) {
            arr.push(key);
            arr1.push(p[key]);
          }
        }
        var temp = -1;
        $.each(data.dulieu, function (i, l) {
          sum += l;
          temp += 1;
          var colorpoint1 = '';
          if (i == arr[temp]) {
            colorpoint1 = arr1[temp];
          }
          $('.function-apend').append('<div class=" box_tam col-6 col-md-3">\n' +
            '<div align="center" class="chart--box box--1" style="background-color:' + colorpoint1 + '!important;">\n' +
            '<p class="text-white box--1--txt">' + i + '</p>\n' +
            '<p class="text-white box--1--num">' + l + '</p>\n' +
            '</div>\n' +
            '</div> ');
        });
        document.getElementById('sumNum').innerHTML = sum;
      });
    },
    error: function (a) {
      console.log(a)
    }
  });

  // CHART 2
  $('#datepicker2').datepicker({
    autoclose: true,
    format: "MM",
    startView: "months",
    minViewMode: "months"
  }).on("change", function () {
    var month = $(this).datepicker('getDate').getMonth() + 1;
    var year = $(this).datepicker('getDate').getFullYear();
    month1_temp = month;
    $.ajax({
      url: ip + '/api/statitic',
      type: 'POST',
      data: {
        "month": month,
        "year": year,
        "function": dl
      },
      success: function (data) {
        $('.delete').trigger('click');
        Highcharts.chart('container2', {
          chart: {
            type: 'column'
          },
          title: {
            text: ' '
          },
          xAxis: {
            categories: data.created,
            labels: {
              style: {
                fontWeight: 'bold',
              },
            },
          },
          yAxis: {
            min: 0,
            title: {
              text: ' '
            }
          },
          tooltip: {
            shadow: false,
            backgroundColor: '#4a4a4a',
            borderColor: '#4a4a4a',
            borderRadius: 20,
            style: {
              color: '#ffffff',
            },
            useHTML: true,
            headerFormat: '<div class="mytooltip"> <div class="row">\n' +
            '<div class="col-7 d-flex flex-wrap align-self-center align-items-center" style="border-right:1px solid #ffffff; margin-top:0px;">\n',
            pointFormat: '<div class="col-6"><i class="fas fa-circle" style="color:{series.color}"></i>&nbsp;{point.y}</div>',
            footerFormat: '</div>\n' +
            '<div class="col-5">\n' +
            '<b style="font-size: 22px;">{point.total}</b> <br>' +
            '<b style="font-size: 12px;font-family: KPMG">{point.x}' + "/" + mm_1 + "/" + yy + '</b> <br>' +
            '</div>\n' +
            '</div>\n' +
            '</div>',
            shared: true,
          },
          plotOptions: {
            column: {
              stacking: 'normal'
            }
          }, legend: {
            reversed: true
          },
          series: data.data
        });
        $(document).ready(function () {
          $('.function-apend1').empty();
          var sum = 0;
          var p = data.data;
          var arrname = [];
          var arrcol = [];
          for (var key in p) {
            if (p.hasOwnProperty(key)) {
              arrname.push(p[key].name);
              arrcol.push(p[key].color);
            }
          }
          var temp = -1;
          $.each(data.dulieu, function (i, l) {
            temp += 1;
            var colorpoint1 = '';
            if (i == arrname[temp]) {
              colorpoint1 = arrcol[temp];
            }
            $('.function-apend1').append('<div class="col-6 col-md-3">\n' +
              '<div align="center" class="chart--box box--1" style="background-color:' + colorpoint1 + '!important;">\n' +
              '<p class="text-white box--1--txt">' + i + '</p>\n' +
              '<p class="text-white box--1--num">' + l + '</p>\n' +
              '</div>\n' +
              '</div> ');
          });
          document.getElementById('sumNum2').innerHTML = sum;
        });
      }
    });
  });

  $.ajax({
    url: ip + '/api/statitic',
    type: 'POST',
    data: {
      "month": mm,
      "year": yyyy,
      "function": dl
    },
    success: function (data) {
      Highcharts.chart('container2', {
        chart: {
          renderTo:container2,
          type: 'column'
        },
        title: {
          text: ' '
        },
        xAxis: {
          categories: data.created,
          labels: {
            style: {
              fontWeight: 'bold',
            },
          },
        },
        yAxis: {
          min: 0,
          title: {
            text: ' '
          }
        },
        tooltip: {
          shadow: false,
          backgroundColor: '#4a4a4a',
          borderColor: '#4a4a4a',
          borderRadius: 20,
          style: {
            color: '#ffffff',
          },

          useHTML: true,
          headerFormat: '<div class="mytooltip"> <div class="row">\n' +
          '<div class="col-7 d-flex flex-wrap align-self-center align-items-center" style="border-right:1px solid #ffffff; margin-top:0px;">\n',
          pointFormat: '<div class="col-6"><i class="fas fa-circle" style="color:{series.color}"></i>&nbsp;{point.y}</div>',
          footerFormat: '</div>\n' +
          '<div class="col-5">\n' +
          '<b style="font-size: 22px;">{point.total}</b> <br>' +
          '<b style="font-size: 12px; font-family: KPMG">{point.x}' + "/" + mm_1 + "/" + yy + '</b> <br>' +
          '</div>\n' +
          '</div>\n' +
          '</div>',
          shared: true,
        },
        plotOptions: {
          column: {
            stacking: 'normal'
          }
        }, legend: {
          reversed: true
        },
        series: data.data
      });
      $(document).ready(function () {
        var sum = 0;
        var p = data.data;
        var arrname = [];
        var arrcol = [];
        for (var key in p) {
          if (p.hasOwnProperty(key)) {
            arrname.push(p[key].name);
            arrcol.push(p[key].color);
          }
        }
        var temp = -1;
        $.each(data.dulieu, function (i, l) {
          temp += 1;
          var colorpoint1 = '';
          if (i == arrname[temp]) {
            colorpoint1 = arrcol[temp];
          }
          $('.function-apend1').append('<div class="" style="width: 20%" >\n' +
            '<div align="center" class="chart--box box--1" style="background-color:' + colorpoint1 + '!important;">\n' +
            '<p class="text-white box--1--txt">' + i + '</p>\n' +
            '<p class="text-white box--1--num">' + l + '</p>\n' +
            '</div>\n' +
            '</div> ');
        });
        document.getElementById('sumNum2').innerHTML = sum;
      });
    }
  });

  // CHART 3
  $('#datepicker3').datepicker({
    autoclose: true,
    format: "MM",
    startView: "months",
    minViewMode: "months"
  }).on("change", function () {
    var month = $(this).datepicker('getDate').getMonth() + 1;
    var year = $(this).datepicker('getDate').getFullYear();
    month2_temp = month;
    var txt_month;
    switch (month) {
      case 1:
        txt_month = "January";
        break;
      case 2:
        txt_month = "February";
        break;
      case 3:
        txt_month = "March";
        break;
      case 4:
        txt_month = "April";
        break;
      case 5:
        txt_month = "May";
        break;
      case 6:
        txt_month = "June";
        break;
      case 7:
        txt_month = "July";
        break;
      case 8:
        txt_month = "August";
        break;
      case 9:
        txt_month = "September";
        break;
      case 10:
        txt_month = "October";
        break;
      case 11:
        txt_month = "November";
        break;
      case 12:
        txt_month = "December";
        break;
    }
    document.getElementById('sumNum3').innerHTML = txt_month;
    // document.getElementById('sumNum4').innerHTML = txt_month;
    $.ajax({
      url: ip + '/api/statitic/chart3',
      type: 'POST',
      data: {
        "month": month,
        "year": year,
        "function": dl
      },
      success: function (data) {
        $('.delete').trigger('click');
        Highcharts.chart('container3', {
          chart: {
            type: 'column'
          },
          title: {
            text: ''
          },
          subtitle: {
            text: ''
          },
          xAxis: {
            lineColor: '#dbdbdb',
            lineWidth: 1,
            categories: data.created,
            crosshair: true,
            labels: {
              style: {
                fontWeight: 'bold',
                fontSize: '15px',
              },
            },
          },
          yAxis: {
            lineColor: '#dbdbdb',
            lineWidth: 1,
            min: 0,
            title: {
              text: ' '
            }
          },
          tooltip: {
            shadow: false,
            backgroundColor: '#4a4a4a',
            borderColor: '#4a4a4a',
            borderRadius: 20,
            style: {
              color: '#ffffff',
            },
            useHTML: true,
            headerFormat: '<b>{point.key}<br></b>',
            formatter: function () {
              html = '<div class="mytooltip"> <div class="row">\n' +
                '<div class="col-7 d-flex flex-wrap align-self-center align-items-center" style="border-right:1px solid #ffffff;">\n';
              var colorpoint = '';
              var temp = -1;
              var p = data.color;
              var arrmau = [];
              var arr1 = [];
              for (var key in p) {
                if (p.hasOwnProperty(key)) {
                  arrmau.push(key);
                  arr1.push(p[key]);
                }
              }
              $.each(this.point.dt, function (k, v) {
                temp++;
                if (k == arrmau[temp]) {
                  colorpoint = arr1[temp];
                }
                html += '<div class="col-6"><i class="fas fa-circle" style="color:' + colorpoint + '"></i>&nbsp;' + v + '&nbsp;&nbsp;</div>';
              });
              html += '</div>\n' +
                '<div class="col-5">\n' +
                '<b style="font-size: 22px;">' + this.y + '</b> <br>' +
                '<a style="font-size: 12px"> ' + this.x + '</a>' +
                '</div>\n' +
                '</div>\n' +
                '</div>';
              return html;
            }
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: data.data,
        });
        $(document).ready(function () {
          //mau
          $('.function-apend2').empty();
          var p = data.dulieu;
          var arr = [];
          var arr1 = [];
          for (var key in p) {
            if (p.hasOwnProperty(key)) {
              arr.push(key);
              arr1.push(p[key]);
            }
          }
          var datas = data.data;
          $.each(arr, function (key, value) {
            var index = datas.findIndex((ar) => {
              return ar.name == value
            });
            arr1.sort(function (a, b) {
              if (a < b) {
                return 1;
              }
              else if (a == b) {
                return 0;
              }
              else {
                return -1;
              }
            });
            if (key <= 2) {
              $('.function-apend2').append('<div class="col-4 height--box">\n' +
                '<div align="center" class=" d-flex chart--box chart3--box box--1 h-100" style="background-color:' + datas[index].color + '!important;">\n' +
                '<div class="border--icon m-3"></div>' +
                '<div class="w-100 align-content-between d-flex flex-wrap justify-content-center mr-2"><p class="text-white box--2--txt">' + arr[key] + '</p>\n' +
                '<p class="text-white box--2--num">' + arr1[key] + '</p>\n' +
                '</div></div></div>');
            }
          });
        });
      }
    });
  });
  $.ajax({
    url: ip + '/api/statitic/chart3',
    type: 'POST',
    data: {
      "month": mm,
      "year": yyyy,
      "function": dl
    },
    success: function (data) {
      // console.log(data)
      Highcharts.chart('container3', {
        chart: {
          renderTo: 'container3',
          type: 'column'
        },
        title: {
          text: ''
        },
        subtitle: {
          text: ''
        },
        xAxis: {
          lineColor: '#dbdbdb',
          lineWidth: 1,
          categories: data.created,
          title: {
            text: '<a id="sumNum4">' + txt_month1 + '</a>',
            style: {
              fontWeight: 'bold',
              fontSize: '12px'
            },
          },
          crosshair: true,
          labels: {
            style: {
              fontWeight: 'bold',
              fontSize: '12px'
            },
          },
        },
        yAxis: {
          lineColor: '#dbdbdb',
          lineWidth: 1,
          min: 0,
          title: {
            text: ' '
          },

        },
        tooltip: {
          shadow: false,
          backgroundColor: '#4a4a4a',
          borderColor: '#4a4a4a',
          borderRadius: 20,
          style: {
            color: '#ffffff',
          },
          useHTML: true,
          headerFormat: '<b>{point.key}<br></b>',
          formatter: function () {
            html = '<div class="mytooltip"> <div class="row">\n' +
              '<div class="col-7 d-flex flex-wrap align-self-center align-items-center" style="border-right:1px solid #ffffff;">\n';
            var colorpoint = '';
            var temp = -1;
            var p = data.color;
            var arrmau = [];
            var arr1 = [];
            for (var key in p) {
              if (p.hasOwnProperty(key)) {
                arrmau.push(key);
                arr1.push(p[key]);
              }
            }
            $.each(this.point.dt, function (k, v) {
              temp++;
              if (k == arrmau[temp]) {
                colorpoint = arr1[temp];
              }
              html += '<div class="col-6"><i class="fas fa-circle" style="color:' + colorpoint + '"></i>&nbsp;' + v + '&nbsp;&nbsp;</div>';
            });
            html += '</div>\n' +
              '<div class="col-5">\n' +
              '<b style="font-size: 22px;">' + this.y + '</b> <br>' +
              '<a style="font-size: 12px"> ' + this.x + '</a>' +
              '</div>\n' +
              '</div>\n' +
              '</div>';
            return html;
          }
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        series: data.data,
      });
      $(document).ready(function () {
        var p = data.dulieu;
        var arr = [];
        var arr1 = [];
        for (var key in p) {
          if (p.hasOwnProperty(key)) {
            arr.push(key);
            arr1.push(p[key]);
          }
        }
        var datas = data.data;
        $.each(arr, function (key, value) {
          var index = datas.findIndex((ar) => {
            return ar.name == value
          });
          arr1.sort(function (a, b) {
            if (a < b) {
              return 1;
            }
            else if (a == b) {
              return 0;
            }
            else {
              return -1;
            }
          });
          if (key <= 2) {
            $('.function-apend2').append('<div class="height--box" style="width: 30%">\n' +
              '<div align="center" class=" d-flex chart--box chart3--box box--1 h-100" style="background-color:' + datas[index].color + '!important;">\n' +
              '<div class="border--icon m-3"></div>' +
              '<div class="w-100 align-content-between d-flex flex-wrap justify-content-center mr-2"><p class="text-white box--2--txt">' + arr[key] + '</p>\n' +
              '<p class="text-white box--2--num">' + arr1[key] + '</p>\n' +
              '</div></div></div>');
          }
        });
      });
    }
  });
}
