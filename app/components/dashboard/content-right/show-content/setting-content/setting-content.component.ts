import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SettingService} from '../../../../../services/setting/setting.service';
import {User} from '../../../../../models/setting/user';
import swal from 'sweetalert';
import {ChangeDetectorRef} from '@angular/core';
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';

declare var $: any;
declare var setHeightElement: any;
declare var setTagZone: any;

@Component({
  selector: 'app-setting-content',
  templateUrl: './setting-content.component.html',
  styleUrls: ['./setting-content.component.css']
})
export class SettingContentComponent implements OnInit, OnDestroy, AfterViewInit {
  public zones: any = '';
  public roles: any = '';
  public users: any = '';
  // zone
  public tags: any = '';
  public scoreArr: any = [];
  public zoneByID: any;
  public idZone: any;
  // Ytu
  public listYitu: any = '';
  public adUser: User = new User();
  public upUser: User = new User();

  public IP: any = '';
 // is show loading table user
  public isShowTableUser = true;
  public isShowTableZone = true;
  public isShowTableYitu = true;

  constructor(public settingService: SettingService, private chRef: ChangeDetectorRef, private config: ConfigIpService) {
    this.IP = config.getIp();
  }

  ngOnInit() {
    this.setHeigth('#setting-content');
    $('.myTags').select2({
      allowClear: true
    });
    $('#favorite_update').select2({
      allowClear: true,
    });
    this.getZones();
    this.getUsers();
    this.getYitu();
  }
  setHeigth(ele) {
    const height = $(window).height() - $(ele).offset().top - 20;
    $(ele).css('height', height);
  }

  getUsers() {
    this.settingService.getUsersSetting().subscribe(res => {
      this.isShowTableUser = false ;
      this.destroyDataTable('#table_user');
      this.users = res.data;
      this.dataTablesConfig('#table_user', '#navigation_user');
    }, err => {
      console.log(err);
    });
  }

  getRole() {
    this.settingService.getAllRoles().subscribe(res => {
      this.roles = res.data;
    }, error => {
      console.log(error);
    });
  }

  addUser() {
    const array = [];
    $('#list-permission input:checkbox:checked').map(function () {
      array.push($(this).val());
    }).get();
    this.adUser.permission = array;
    this.settingService.addUserSetting(this.adUser).subscribe(res => {
      swal('Add User Success!', '', 'success');
      this.getUsers();
      this.resetUserVaribleAdd();
    }, err => {
      swal('Email is Exist!', '', 'error');
    });
  }


  updateUser(id?) {
    const array = [];
    $('#list-permission-update input:checkbox:checked').map(function () {
      array.push($(this).val());
    }).get();
    this.upUser.permission = array;
    this.settingService.updateUserSetting(this.upUser).subscribe(res => {
      swal('Update Success!', '', 'success');
      this.getUsers();
      this.resetPassword();
    }, err => {
      swal('Update Failure!', '', 'error');
    });
  }

  resetPassword() {
    this.upUser.password = '';
  }

  resetUserVaribleAdd() {
    this.adUser.full_name = '';
    this.adUser.password = '';
    this.adUser.email = '';
    this.adUser.phone = '';
  }

  deleteUser(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this user information!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willDelete) => {
        if (willDelete) {
          this.settingService.deleteUserSetting(id).subscribe(res => {
            this.getUsers();
            swal('Delete Success!', '', 'success');
          }, err => {
            swal('Not Delete!', '', 'error');
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

  getUserById(id) {
    this.getRole();
    this.settingService.getUSerSettingById(id).subscribe(res => {
      this.assignUpDateUserVarible(res.data);
    }, err => {
      console.log(err);
    });
  }

  assignUpDateUserVarible(data) {
    this.upUser.id = data.Employment.id;
    this.upUser.full_name = data.Employment.full_name;
    this.upUser.phone = data.Employment.phone;
    this.upUser.email = data.Employment.email;
    $('#list-permission-update input:checkbox').map(function () {
      const that = $(this);
      for (let i = 0; i < data.role.length; i++) {
        const valueOfRole = '' + data.role[i];
        if (that.val() === valueOfRole) {
          that.attr('checked', true);
          break;
        }
      }
    });
  }

  // Zones
  getTagAll() {
    this.settingService.getAllTag().subscribe(res => {
      this.tags = res.data;
    }, err => {
      console.log(err);
    });
  }

  getZones() {
    this.settingService.getZonesSetting().subscribe(res => {
      this.isShowTableZone = false;
      this.destroyDataTable('#table_zone');
      this.zones = res.data;
      // config dataTables
      this.dataTablesConfig('#table_zone', '#navigation_zone');
    }, err => {
      console.log(err);
    });
  }

  addZone(code, name, color, static_color, top_from, top_to, left_from, left_to, description) {
    console.log(top_from);
    console.log(top_to);
    console.log(left_from);
    console.log(left_to);
    const score = this.getScoreOject('.score_tag', 3);
    this.settingService.addZone({
      'code': code,
      'name': name,
      'description': description,
      'favorite': score,
      'color': color,
      'color_statitic': static_color,
      'top_from': top_from,
      'top_to': top_to,
      'left_from': left_from,
      'left_to': left_to
    }).subscribe(res => {
      this.getZones();
      swal('Add Zone Success!', '', 'success');
    }, err => {
      swal('Can Not Add New Zone!', '', 'error');
    });
    $('.myTags').select2().val(null).trigger('change');

  }

  getZoneByID(id) {
    this.idZone = id;
    console.log(this.idZone);
    this.getTagAll();
    this.settingService.getZoneById(id).subscribe(res => {
      this.zoneByID = res.data;
      console.log(this.zoneByID);
      this.setFieldZoneUpdate();
    }, err => {
      console.log(err);
    });
  }

  setFieldZoneUpdate() {
    this.setFieldScoreUpdate();
    setTagZone(this.idZone, this.IP);
    $('#code_zone_update').val(this.zoneByID.code);
    $('#name_zone_update').val(this.zoneByID.name);
    $('#zone_color_update').val(this.zoneByID.color);
    $('#static_color_update').val(this.zoneByID.color_statitic);
    $('#top_from_update').val(this.zoneByID.top_from);
    $('#top_to_update').val(this.zoneByID.top_to);
    $('#left_from_update').val(this.zoneByID.left_from);
    $('#left_to_update').val(this.zoneByID.left_to);
    $('#description_update').val(this.zoneByID.description);
  }
  getArrayTag(arr) {
    // this.setFieldScoreUpdate();
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
      newArr.push(arr[i].id);
    }
    return newArr;
  }

  setFieldScoreUpdate() {
    let html = '';
    // const arrTag = [];
    $.each(this.zoneByID.favorites, function (key, val) {
      // arrTag.push(val.id);
      console.log(val.id);
      html += '<li><div class="row" style="align-items: center">' + '<span class="col-sm-3 ml-2">' + val.name + '</span>' + '<div class="input-group col-sm-4" style="align-items: center">' + '<input class="form-control score_tag_update" id="tag_update' + val.id + '" type="text" value="' + val.score + '">' + '<span class="" style="margin-left: 15px">' + 'score' + '</span>' + '</div>' + '</div></li>';
    });
    $('#result_update').html(html);
    const val_tag = $('#favorite_update').val();
    if (val_tag === '') {
      $('#plus_score_update').css('display', 'none');
    } else {
      $('#plus_score_update').css('display', 'block');
    }
  }


  updateZone(code, name, color, static_color, top_from, top_to, left_from, left_to, description) {
    const score = this.getScoreOject('.score_tag_update', 10);
    // console.log(score);
    // console.log(code);
    // console.log(name);
    // console.log(color);
    // console.log(static_color);
    // console.log(top_from);
    // console.log(top_to);
    // console.log(left_from);
    // console.log(left_to);
    // console.log(description);
    // console.log(score);
    this.settingService.updateZone(this.idZone, {
      'code': code,
      'name': name,
      'description': description,
      'favorite': score,
      'color': color,
      'color_statitic': static_color,
      'top_from': top_from,
      'top_to': top_to,
      'left_from': left_from,
      'left_to': left_to
    }).subscribe(res => {
      this.getZones();
      swal('Update Zone Success!', '', 'success');
    }, err => {
      swal('Update Zone Failure!', '', 'error');
    });
  }

  getScoreOject(obj, num) {
    const that = this;
    const scoreArray = [];
    $(obj).each(function () {
      const key = $(this).attr('id');
      const key_id = key.slice(num);
      const value = $(this).val();
      scoreArray.push({
        key: key_id,
        value: value
      });
    });
    // convert
    const result = {};
    for (let i = 0; i < scoreArray.length; i++) {
      result[scoreArray[i].key] = scoreArray[i].value;
    }
    return result;
  }

  setScoreArray(obj) {
    const that = this;
    $(obj).each(function () {
      const key = $(this).attr('id');
      that.scoreArr.push(key);
    });
  }

  getOldScore(arr) {
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
      newArr.push({
        key: arr[i].id,
        value: arr[i].score
      });
    }
    console.log(newArr);
    // convert
    const result = {};
    for (let i = 0; i < newArr.length; i++) {
      result[newArr[i].key] = newArr[i].value;
    }
    return result;
  }

  deleteZone(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Zone!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.settingService.deleteZone(id).subscribe(res => {
            this.getZones();
            swal('Delete Success!', '', 'success');
          }, err => {
            swal('Not Delete!', '', 'error');
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

  // yitu
  getYitu() {
    this.settingService.getListYitu().subscribe(res => {
      this.isShowTableYitu = false;
      this.listYitu = res.data;
      // config dataTables
      this.dataTablesConfig('#table_yitu', '#navigation_yitu');
    }, err => {
      console.log(err);
    });
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
  }

  // dataTable function
  dataTablesConfig(objTable, objPagi) {
    // You'll have to wait that changeDetection occurs and projects data into
    // the HTML template, you can ask Angular to that for you ;-)
    this.chRef.detectChanges();
    $(objTable).DataTable({
      'pageLength': 10
    });

    this.setPagiInfo(objTable, objPagi);
    this.actionPagiPage(objTable, objPagi);
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }

  setPagiInfo(objTable, objPagi) {
    const table = $(objTable).DataTable();
    if (table && table.page.info()) {
      const currentPage = table.page.info().page + 1;
      const totalPage = table.page.info().pages;
      const pagi = 'Page ' + currentPage + ' of ' + totalPage;
      $(objPagi + ' .pagin').html(pagi);
    } else {
      $(objPagi + ' .pagin').html('Page 1 of 1');
    }
  }

  actionPagiPage(objTable, objPagi) {
    const that = this;
    $(objTable + ' th').on('click', function () {
      that.setPagiInfo(objTable, objPagi);
    })
    // action pagination page
    const table = $(objTable).DataTable();
    $(objPagi + ' .ic_next').on('click', function () {
      table.page('next').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
    $(objPagi + ' .ic_prev').on('click', function () {
      table.page('previous').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
  }
}
