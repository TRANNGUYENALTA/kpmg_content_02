import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FeedsService} from '../../../../../services/feeds/feeds.service';
import './../../../../../../assets/js/feed.js';
import {Feed, FeedInfor, RuleFeedInformation} from '../../../../../models/feed/feed';
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';
import {RuleService} from '../../../../../services/rules/rule.service';

// Data Table
import {ChangeDetectorRef} from '@angular/core';
import swal from 'sweetalert';

declare var $: any;
declare var setHeightElement: any;
declare var feedFunction: any;
declare var monthClick: any;
declare var getselect2: any;
declare var setTag: any;
// check image
declare var checkImageFeed: any;
declare var checkImagePromo: any;
declare var checkImageFeedInfo: any;
declare var checkImagePromoTemplate02: any;
declare var checkImagePromoTemplate03: any;
declare var checkVideoSizeTemplate: any;
declare var checkImageLogoTemplate02: any;
declare var isImageFeed: any;
declare var isImagePromo: any;
declare var isImagePromTemplate_02: any;
declare var isImagePromTemplate_03: any;
declare var isVideoPromTemplate_03: any;
declare var isImageLogoPromTemplate_02: any;
declare var isImageFeedInfo: any;
declare var css: any;
declare var show_icon: any;
declare var show_anypicker: any;
declare var convertImageTopBase64: any;
declare var convertImageBottomoBase64: any;
declare var convertVideoBase64: any;
declare var escapeRegExp: any;


declare var removeclass: any;
declare var show_table_emojiAddfeed: any;
// get file base64 from variable in dashboard js
declare var base64StringTopInJs: any;
declare var base64StringBottomInJs: any;
declare var gradientImageTop: any;
declare var gradientImageBottom: any;
// draw canvas
declare var setCanVasToLaunch: any;

@Component({
  selector: 'app-feeds-content',
  templateUrl: './feeds-content.component.html',
  styleUrls: ['./feeds-content.component.css']
})

export class FeedsContentComponent implements OnInit, OnDestroy, AfterViewInit {
  public subcription1: Subscription;
  public subcription2: Subscription;
  public dataMonth: any = '';
  public feed: Feed = new Feed();
  public updateFeed: Feed = new Feed();
  public feedInfor: FeedInfor = new FeedInfor();
  public feedInforUpdate: FeedInfor = new FeedInfor();
  public idFeedInfo: any;
  public oneFeedInfo: any = '';
  public feedById: any = '';
  public input = new FormData();
  public inputUpdate = new FormData();
  public inputFeedInfo = new FormData();
  public inputFeedRule = new FormData();
  public inputFeedInfoUpdate = new FormData();
  public isUpdate: any = true;
  public idUpdate: any;
  // Config domain
  public IP: any = '';

  // Feed Information
  public allFeedInfo: any = '';
  // check image
  public isFeedImage: any = true;
  public isFeedInfoImage: any = true;

  // Feed Information Rules
  public condition: any = '';
  public operatorr: any = '';
  public feedRules: any = '';
  public isAddImage: any = true;
  public showImage: any = 'd-block';
  public showIcon: any = 'd-none';
  public feedParentId: any;
  public ruleId: any;
  public isAddRuleAction: any;
  public titleRuleAction: any = 'Add Rule Feed Information';
  public feedInfoRule: RuleFeedInformation = new RuleFeedInformation();

  // isEvent
  public isActiveEvent: any = false;
  // Loading
  public isLoadingCalendar: any = true;
  // Promo
  public idTemplate: any = '';
  public colorTopHeader_top: any = '#000000';
  public colorTopHeader_bottom: any = '#000000';

  // header
  public headerTop: any = 'Header Display Here (37 characters)';
  public headerBottom: any = 'Header Display Here (37 characters)';
  // isTop or isBottom
  public isTop: any = 'd-block';
  public isBottom: any = 'd-none';
// top choose
  public isTopChoose: any = true;
  public isBottomChoose: any = false;
  // Templates
  public templates: any = '';
  // icon template
  public icon_type01: any = '';
  public icon_type02: any = '';
  public iconTopUrl: any = '';
  public iconBottomUrl: any = '';
  public idIconTop: any = null;
  public idIconBottom: any = null;
  // image template
  public imgTemplateType1: any = '';
  public imgTemplateType2: any = '';
// check image add true or false
  public isImageTop: any = true;
  public isImageBottom: any = true;
  public isImagePromotemplate02: any = true;
  public isImageLogotemplate02: any = true;
  public isImagePromotemplate03: any = true;
  public isVideoPromotemplate03: any = true;
// disabled div
  public step_02 = 'disableDiv';
  public step_03 = 'disableDiv';
// check stept 2
  public isDoneStep2: any = 'disableDiv';
  public checkImgTop: any = false;
  public checkImgBottom: any = false;
// Send active template promo
  public isActiveSendActive: any = false;
  public isActiveTemplateUpdate: any = false;
  public idtemplateJustAdd: any = '';
  public isDoneStep3: any = false;
// update template
  public titlePromo: any = 'Select Template';
  public isUpdatePromo: any = false;
  public template_id: any = '';
// id template detail
  public id_1_templateDetail: any = '';
  public id_2_templateDetail: any = '';
  // choose template
  public template_01: any = 'd-none';
  public template_02: any = 'd-none';
  public template_03: any = 'd-none';

  //  variable for template 02
  public header_temp_02: any = 'KPMG Welcomes';
  public subtitle_temp_02: any = 'MD of ABC Health';
  public description_temp_02: any = 'Dr. John Loh';
  public colorHeaderTemplate_02: any = '#000000';
  public colorSubtitleTemplate_02: any = '#000000';
  public colorDescriptionTemplate_02: any = '#000000';

  public isCheckLogo: any = true;
// variable for template 03
  public isTopTemp03: any = '';
  public isBottomTemp03: any = 'd-none';
  public urlLogoTempalte03: any = '';
  public idLogoTempalte03: any = '';
  public logosTemplate03: any = '';

  // public choose radient
  public isChooseRadient: any = 'disable';
  public isChooseImage: any = '';
  public isChooseRadientBottom: any = 'disable';
  public isChooseImageBottom: any = '';
  // gradient choosed
  public gradientChoosed: any = '';
  public gradientChoosedBottom: any = '';
  // gradient top and body
  public gradientTop: any = '';
  public gradientBottom: any = '';

  constructor(public feedService: FeedsService,
              private chRef: ChangeDetectorRef,
              private config: ConfigIpService,
              private ruleService: RuleService) {
    this.IP = config.getIp();
  }

  ngAfterViewInit() {
    const that = this;
    // // Active button event
    // $(document).on('click', '#button_event', function () {
    //   that.eventAction();
    // });
  }

  ngOnInit() {
    const that = this;
    setHeightElement('#external');
    removeclass();
    show_anypicker();
    show_table_emojiAddfeed('addFeed');
    show_table_emojiAddfeed('updateFeed');
    // check image Feed
    checkImageFeed('#margin');
    checkImageFeed('#margin_update');
    // check image Promo
    checkImagePromo('#filePromo');
    checkImagePromo('#filePromo_bottom');
    checkImagePromoTemplate02('#filePromo_02');
    checkImageLogoTemplate02('#fileLogoPromo_02');
    // Check size file upload template 03
    checkVideoSizeTemplate('#fileVideoPromo_03', 600, 338);
    checkImagePromoTemplate03('#filePromo_03');
    // check image Feed Information
    checkImageFeedInfo('#file_infor_feed');
    checkImageFeedInfo('#file_infor_feed_update');
    checkImageFeedInfo('#blah0_feedInfoRule');
    $('.carousel').carousel({
      interval: 0
    });
    // Remove event on calendar and on month
    $(document).on('click', '.remove_event', function () {
      that.isUpdate = false;
      const id = $(this).data('id');
      that.deleteEvent(id);
    });
    // Check logo
    $(document).on('click', '#logo_template_02', function () {
      that.isCheckLogo = !that.isCheckLogo;
      if (that.isCheckLogo) {
        $(this).prop('checked', true);
        $('.logo_bottom_template_02').removeClass('d-none').addClass('d-block');
      } else {
        $(this).prop('checked', false);
        $('.logo_bottom_template_02').removeClass('d-block').addClass('d-none');
      }
    });
    // end check logo
    $(document).on('click', '.remove_item', function () {
      that.isUpdate = false;
    });
    // End remove event on calendar and on month
    // Reset to Add promo
    $(document).on('click', '#add_feed_button', function () {
      that.isUpdatePromo = false;
      $('#step1').click();
      this.titlePromo = 'Select Template';
      that.resetStep2();
      $('.content-item_01').removeClass('active');
    });
    // End reset to add promo
    // Click update event on calendar
    $(document).on('click', '.content_event', function () {
      const type = $(this).data('type');
      const template_id = $(this).data('template');
      if (type === 1) {
        if (that.isUpdate) {
          const id = $(this).children('.remove_event').data('id');
          that.idUpdate = id;
          that.getFeedById(id);
          $('#modal_updateFeed').click();
        }
      } else {
        if (that.isUpdate) {
          that.template_id = template_id;
          that.isUpdatePromo = true;
          that.titlePromo = 'Update Template';
          that.isActiveTemplateUpdate = false;
          $('.button_active_template ').removeClass('active');
          const id = $(this).children('.remove_event').data('id');
          that.idUpdate = id;
          that.getPromoById(id);
          $('#modal_updatePromo').click();
        }
      }
      that.isUpdate = true;
    });
    // End update event on calendar
    $(document).on('click', '.item_list', function () {
      const type = $(this).data('type');
      const template_id = $(this).data('template');
      if (type === 1) {
        if (that.isUpdate) {
          console.log(that.idUpdate);
          that.getFeedById(that.idUpdate);
          $('#modal_updateFeed').click();
        }
      } else {
        if (that.isUpdate) {
          that.template_id = template_id;
          that.isUpdatePromo = true;
          console.log(template_id);
          that.titlePromo = 'Update Template';
          that.getPromoById(that.idUpdate);
          $('#modal_updatePromo').click();
        }
      }

      that.isUpdate = true;
    });

    // show tempalte
    this.showTemplateDemo('#click_note', '#content-note', 1);
    this.showTemplateDemo('#click_note_bottom', '#content-note-bottom', 2);
    this.showTemplateDemo('#click_note_02', '#content-note_02', 1);
    this.showTemplateDemo('#click_note_03', '#content-note_03', 1);

    // end update envent on feed month
    // template
    // Action step 01 select template
    $(document).on('click', '.content-item_01', function () {
      $('.content-item_01').removeClass('active');
      $(this).addClass('active');
      that.getIconTop();
      $('#step2').click();
    });
    // End select template
    // active top and bottom template
    $(document).on('click', '.group_item', function () {
      $('.group_item').removeClass('active');
      $(this).addClass('active');
    });
    $(document).on('click', '.temp_03', function () {
      $('.temp_03').removeClass('active');
      $(this).addClass('active');
    });
    $(document).on('click', '.one_logo_temp03', function () {
      $('.one_logo_temp03').removeClass('active');
      $(this).addClass('active');
      const url = $(this).data('link');
      const id = $(this).data('id');
      that.urlLogoTempalte03 = url;
      that.idLogoTempalte03 = id;
    });

    $('#carouseStep3').hover(function () {
      that.checkForStep2();
    });
    this.getAllFeed();
    this.getFeedsMonth();
    this.getFeedInfomation();
    // Rules Feed Information
    this.getOperator();
    this.getConditions();
    getselect2(this.IP);

    // Call function promo
    this.getAllTemplate();
    this.getLogoTemplate03();
    // carousel feed
    $('#modal-addPromo .carousel-control-prev .left').css('display', 'block!important');

    $('#modal-addPromo .carousel').on('slid.bs.carousel', function () {
      if ($('#modal-addPromo .carousel-item:first').hasClass('active')) {
        if (!that.isUpdatePromo) {
          that.titlePromo = 'Select Template';
        }
        $('#modal-addPromo .carousel-control-prev').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel-control-next').addClass('d-block').removeClass('d-none');
        $('#modal-addPromo .carousel_control .left').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel_control .right').addClass('d-block').removeClass('d-none');
        // console.log($this.data);
      } else if ($('#modal-addPromo .carousel-item:last').hasClass('active')) {
        if (!that.isUpdatePromo) {
          that.titlePromo = 'Launch Setting';
        }
        $('#modal-addPromo .carousel-control-next').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel-control-prev').addClass('d-block').removeClass('d-none');
        $('#modal-addPromo .carousel_control .right').addClass('d-none').removeClass('d-block');
        $('#modal-addPromo .carousel_control .left').addClass('d-block').removeClass('d-none');
      } else {
        if (!that.isUpdatePromo) {
          that.titlePromo = 'Select Media';
        }

        $('#modal-addPromo .carousel-control-prev').removeClass('d-none').addClass('d-block');
        $('#modal-addPromo .carousel-control-next').removeClass('d-none').addClass('d-block');
        $('#modal-addPromo .carousel-control').removeClass('d-none').addClass('d-block');
      }
    });
  }


  // show template
  showTemplateDemo(objHover, objContent, type) {
    const that = this;
    let isLeave = false;
    // Note download template demo
    $(objHover).mouseover(function () {
      isLeave = true;
      that.getImgTemplateDemo(type);
      $(objContent).removeClass('d-none').addClass('d-inline-block');
    });
    $(objContent).mouseover(function () {
      if (isLeave) {
        $(this).removeClass('d-none').addClass('d-inline-block');
      } else {
        $(this).removeClass('d-inline-block').addClass('d-none');
      }
    });

    $(objContent).mouseleave(function () {
      $(this).removeClass('d-inline-block').addClass('d-none');
    });

  }

  setStep() {
    this.step_02 = 'disableDiv';
    this.step_03 = 'disableDiv';
  }

  // Promo
  setColorTopHeader(top, color) {
    if (top === 1) {
      this.colorTopHeader_top = color;
    } else {
      this.colorTopHeader_bottom = color;
    }
  }  // Promo
  setColorTextTemplate02(point, color) {
    if (point === 1) {
      this.colorHeaderTemplate_02 = color;
    } else {
      if (point === 2) {
        this.colorSubtitleTemplate_02 = color;
      } else {
        this.colorDescriptionTemplate_02 = color;
      }
    }
  }

  // check lenght header
  checkLengthHeader(isTop, input) {
    const str = escapeRegExp(input.value);
    const lengthCut = 36 + input.value.length - str.length + 1;
    if (isTop === 1) {
      if (str.length > 36) {
        input.value = input.value.slice(0, lengthCut);
        this.headerTop = input.value;
      }
    } else {
      input.value = input.value.slice(0, lengthCut);
      this.headerBottom = input.value;
    }
  }

  checkLengthString(input, length, pos) {
    const str = escapeRegExp(input.value);
    // input.value = input.value.trim();
    const lengthCut = length + input.value.length - str.length;
    if (str.length > length) {
      input.value = input.value.slice(0, lengthCut);
      if (pos === 1) {
        this.header_temp_02 = input.value;
      }
      if (pos === 2) {
        this.subtitle_temp_02 = input.value;
      }
      if (pos === 3) {
        this.description_temp_02 = input.value;
      }
    }
  }

  topClick(isTemp03?) {
    if (isTemp03 !== 3) {
      const that = this;
      this.isTop = 'd-block';
      this.isBottom = 'd-none';
      this.isTopChoose = true;
      this.isBottomChoose = false;
      this.getIconTop();
      setTimeout(function () {
        if (that.idIconTop) {
          $('#icon_' + that.idIconTop).click();
        }
      }, 70);
    } else {
      this.isTopTemp03 = '';
      this.isBottomTemp03 = 'd-none';
    }
  }

  bottomClick(isTemp03?) {
    if (!isTemp03) {
      const that = this;
      this.isBottom = 'd-block ';
      this.isTop = 'd-none';
      this.isTopChoose = false;
      this.isBottomChoose = true;
      this.getIconBottom();
      setTimeout(function () {
        if (that.idIconBottom) {
          $('#icon_' + that.idIconBottom).click();
        }
      }, 70);
    } else {
      this.isTopTemp03 = 'd-none';
      this.isBottomTemp03 = '';
    }
  }

  getIdTemplate(id) {
    this.idTemplate = id;
    this.checkTemplateId(id);
  }

  checkTemplateId(id) {
    if (id === 1) {
      this.template_01 = '';
      this.template_02 = 'd-none';
      this.template_03 = 'd-none';
      $('#topLayout').click();
      this.getGradientImages(1, 1);
      this.getGradientImages(1, 2);
      $('.icon_bottom_template').removeClass('bg-transparent');
      $('.icon_top_template').removeClass('bg-transparent');
    } else {
      if (id === 2) {
        this.getGradientImages(2, 1);
        this.header_temp_02 = 'KPMG Welcomes';
        this.subtitle_temp_02 = 'MD of ABC Health';
        this.description_temp_02 = 'Dr. John Loh';

        this.template_01 = 'd-none';
        this.template_02 = '';
        this.template_03 = 'd-none';
      } else {
        $('#top_template_03').click();
        this.getGradientImages(3, 1);
        this.header_temp_02 = 'Welcomes to KPMG Clubhouse!';
        this.subtitle_temp_02 = 'Procurement igNite Submit 2018';
        this.description_temp_02 = 'Fri, 31 August 2018 <br> JW Marriot Hotel Singapore South Beach';
        this.template_01 = 'd-none';
        this.template_02 = 'd-none';
        this.template_03 = '';
      }
    }
  }

  getIconTop() {
    this.feedService.getIconsTemplate(this.idTemplate, 1).subscribe(res => {
      this.icon_type01 = res.data;
    }, err => {
      console.log(err);
    });
  }

  getIconBottom() {
    this.feedService.getIconsTemplate(this.idTemplate, 2).subscribe(res => {
      this.icon_type02 = res.data;
    }, err => {
      console.log(err);
    });
  }

  showIconToTemplate(isTop, url, id) {
    if (isTop === 1) {
      this.iconTopUrl = url;
      this.idIconTop = id;
      $('#icon_top_template').addClass('bg-transparent');
      this.checkForStep2();
    } else {
      this.iconBottomUrl = url;
      this.idIconBottom = id;
      $('#icon_bottom_template').addClass('bg-transparent');
      this.checkForStep2();
    }
  }

// Api for Promo
  // 1 get all template
  getAllTemplate() {
    this.feedService.getAllTemplate().subscribe(res => {
      this.templates = res.data;
    }, err => {
      console.log(err);
    });
  }

  getLogoTemplate03() {
    this.feedService.getLogoTempate03().subscribe(res => {
      this.logosTemplate03 = res.data;
      this.urlLogoTempalte03 = res.data[0].file;
      this.idLogoTempalte03 = res.data[0].id;
    }, err => {
    });
  }

  getImgTemplateDemo(type) {
    this.feedService.getImageDemo(this.idTemplate, type).subscribe(res => {
      if (type === 1) {
        this.imgTemplateType1 = res.data[0].file;
      } else {
        this.imgTemplateType2 = res.data[0].file;
      }
    }, err => {
      console.log(err);
    });
  }

  chooseImage(pos, obj) {
    if (pos === 1) {
      this.isChooseImage = '';
      this.isChooseRadient = 'disable';
    } else {
      if (pos === 2) {
        this.isChooseImageBottom = '';
        this.isChooseRadientBottom = 'disable';
      } else {
        this.isChooseImage = '';
        this.isChooseRadient = 'disable';
      }
    }
    $(obj).css('opacity', 0);
  }

  chooseRadient(pos, obj) {
    if (pos === 1) {
      this.isChooseImage = 'disable';
      this.isChooseRadient = '';
    } else {
      if (pos === 2) {
        this.isChooseImageBottom = 'disable';
        this.isChooseRadientBottom = '';
      } else {
        this.isChooseImage = 'disable';
        this.isChooseRadient = '';
      }
    }
    $(obj).css('opacity', 1);
  }

  gradientChoosedAction(pos, obj, file, id) {
    let typeGradient;
    let from;
    let to;
    const value = id % 4;
    switch (value) {
      case 1: {
        from = 'rgb(100, 43, 115)';
        to = 'rgb(198, 66, 110)';
        break;
      }
      case 2: {
        from = 'rgb(237, 33, 58)';
        to = 'rgb(147, 41, 30)';
        break;
      }
      case 3: {
        from = 'rgb(57, 106, 252)';
        to = 'rgb(41, 72, 255)';
        break;
      }
      case 0: {
        from = 'rgb(69, 104, 220)';
        to = 'rgb(176, 106, 179)';
        break;
      }
    }

    typeGradient = id; // id gradient choosed
    if (pos === 1) {
      $(obj).attr('src', file);
      this.gradientChoosed = typeGradient;
      setCanVasToLaunch(1, from, to);
    } else {
      if (pos === 2) {
        $(obj).attr('src', file);
        this.gradientChoosedBottom = typeGradient;
        setCanVasToLaunch(2, from, to);
      }
    }
    this.checkForStep2();
  }

  getGradientImages(temp, type) {
    // call one times to get 4 gradient
    this.feedService.getGradientImageFunction(temp, type).subscribe(res => {
      if (type === 1) {
        this.gradientTop = res.data;
      } else {
        this.gradientBottom = res.data;
      }
    }, err => {
      console.log(err);
    });
  }

// add Promo Template
  addPromoTemplate_01(fileTop, fileBottom, title, dateStart, dateEnd, timeStart, timeEnd) {
    if (fileTop.files && fileTop.files[0]) {
      convertImageTopBase64(fileTop);
    }
    const that = this;
    const datas = new FormData();
    setTimeout(function () {
      datas.append('template_example_id', that.idTemplate);
      if (that.isChooseRadient) {
        datas.append('template_detail[0][file]', fileTop.files[0]);
        datas.append('template_detail[0][gradient]', '');
      } else {
        datas.append('template_detail[0][file]', '');
        datas.append('template_detail[0][gradient]', that.gradientChoosed);
      }
      if (that.isChooseRadientBottom) {
        datas.append('template_detail[1][file]', fileBottom.files[0]);
        datas.append('template_detail[1][gradient]', '');
      } else {
        datas.append('template_detail[1][file]', '');
        datas.append('template_detail[1][gradient]', that.gradientChoosedBottom);
      }
      const content1 = [
        {
          'text': that.headerTop,
          'text_color': that.colorTopHeader_top
        }
      ];
      const content2 = [
        {
          'text': that.headerBottom,
          'text_color': that.colorTopHeader_bottom
        }
      ];
      datas.append('template_detail[0][type]', '' + 1);
      datas.append('template_detail[0][icon]', that.idIconTop);
      datas.append('template_detail[0][logo]', '');
      datas.append('template_detail[0][content]', JSON.stringify(content1));
      // type 2
      if (that.isChooseRadientBottom) {
        datas.append('template_detail[1][file]', fileBottom.files[0]);
        datas.append('template_detail[1][gradient]', '');
      } else {
        datas.append('template_detail[1][file]', '');
        datas.append('template_detail[1][gradient]', that.gradientChoosedBottom);
      }
      datas.append('template_detail[1][type]', '' + 2);
      datas.append('template_detail[1][icon]', that.idIconBottom);
      datas.append('template_detail[1][logo]', '');
      datas.append('template_detail[1][content]', JSON.stringify(content2));

      that.feedService.addPromo(datas).subscribe(res => {
        const idTemplate = res.data.id;
        that.idtemplateJustAdd = idTemplate; // id
        that.addLaunch(idTemplate, title, dateStart, dateEnd, timeStart, timeEnd);
      }, err => {
        console.log(err);
        swal(err.error.message, '', 'error');
      });
    }, 100);
  }

  updatePromoTemplate01(fileTop, fileBottom, title, dateStart, dateEnd, timeStart, timeEnd) {
    const that = this;
    if (fileTop.files && fileTop.files[0]) {
      convertImageTopBase64(fileTop);
    }
    const datas = new FormData();
    setTimeout(function () {
      datas.append('template_example_id', that.idTemplate);
      if (that.isChooseRadient) {
        if (fileTop.files && fileTop.files[0]) {
          datas.append('template_detail[0][file]', fileTop.files[0]);
        } else {
          datas.append('template_detail[0][file]', '');
        }
        datas.append('template_detail[0][gradient]', '');
      } else {
        datas.append('template_detail[0][file]', '');
        datas.append('template_detail[0][gradient]', that.gradientChoosed);
      }
      if (that.isChooseRadientBottom) {
        if (fileBottom.files && fileBottom.files[0]) {
          datas.append('template_detail[1][file]', fileBottom.files[0]);
        } else {
          datas.append('template_detail[1][file]', '');
        }
        datas.append('template_detail[1][gradient]', '');
      } else {
        datas.append('template_detail[1][file]', '');
        datas.append('template_detail[1][gradient]', that.gradientChoosedBottom);
      }
      const content1 = [
        {
          'text': that.headerTop,
          'text_color': that.colorTopHeader_top
        }
      ];
      const content2 = [
        {
          'text': that.headerBottom,
          'text_color': that.colorTopHeader_bottom
        }
      ];
      datas.append('template_detail[0][id]', '' + that.id_1_templateDetail);
      datas.append('template_detail[0][type]', '' + 1);
      datas.append('template_detail[0][icon]', that.idIconTop);
      datas.append('template_detail[0][logo]', '');
      datas.append('template_detail[0][content]', JSON.stringify(content1));
      // type 2
      datas.append('template_detail[1][id]', '' + that.id_2_templateDetail);
      datas.append('template_detail[1][type]', '' + 2);
      datas.append('template_detail[1][icon]', that.idIconBottom);
      datas.append('template_detail[1][logo]', '');
      datas.append('template_detail[1][content]', JSON.stringify(content2));
      that.feedService.updateProm(datas, that.template_id).subscribe(res => {
        that.updateLaunch(that.template_id, title, dateStart, dateEnd, timeStart, timeEnd);
      }, err => {
        console.log(err);
        swal(err.error.message, '', 'error');
      });
    }, 100);
  }

  addPromoTemplate_02(fileTop, fileBottom, title, dateStart, dateEnd, timeStart, timeEnd) {
    if (fileTop.files && fileTop.files[0]) {
      convertImageTopBase64(fileTop);
    }
    const that = this;
    const datas = new FormData();
    setTimeout(function () {
      datas.append('template_example_id', that.idTemplate);
      if (that.isChooseRadient) {
        datas.append('template_detail[0][file]', fileTop.files[0]);
        datas.append('template_detail[0][gradient]', '');
      } else {
        datas.append('template_detail[0][file]', '');
        datas.append('template_detail[0][gradient]', that.gradientChoosed);
      }
      if (that.isCheckLogo) {
        if (fileBottom.files && fileBottom.files[0]) {
          datas.append('template_detail[0][logo]', fileBottom.files[0]);
        }
      }
      const content = [
        {
          'text': that.header_temp_02,
          'text_color': that.colorHeaderTemplate_02
        },
        {
          'text': that.subtitle_temp_02,
          'text_color': that.colorSubtitleTemplate_02
        },
        {
          'text': that.description_temp_02,
          'text_color': that.colorDescriptionTemplate_02
        }
      ];
      datas.append('template_detail[0][type]', '' + 1);
      datas.append('template_detail[0][icon]', '');
      datas.append('template_detail[0][content]', JSON.stringify(content));
      that.feedService.addPromo(datas).subscribe(res => {
        const idTemplate = res.data.id;
        that.idtemplateJustAdd = idTemplate; // id
        that.addLaunch(idTemplate, title, dateStart, dateEnd, timeStart, timeEnd);
      }, err => {
        console.log(err);
        swal(err.error.message, '', 'error');
      });
    }, 100);
  }

  updatePromoTemplate02(fileTop, fileBottom, title, dateStart, dateEnd, timeStart, timeEnd) {
    const that = this;
    if (fileTop.files && fileTop.files[0]) {
      convertImageTopBase64(fileTop);
    }
    const datas = new FormData();
    setTimeout(function () {
      datas.append('template_example_id', that.idTemplate);
      if (that.isChooseRadient) {
        if (fileTop.files && fileTop.files[0]) {
          datas.append('template_detail[0][file]', fileTop.files[0]);
        } else {
          datas.append('template_detail[0][file]', '');
        }
        datas.append('template_detail[0][gradient]', '');
      } else {
        datas.append('template_detail[0][file]', '');
        datas.append('template_detail[0][gradient]', that.gradientChoosed);
      }
      if (that.isCheckLogo) {
        if (fileBottom.files && fileBottom.files[0]) {
          datas.append('template_detail[0][logo]', fileBottom.files[0]);
        } else {
          datas.append('template_detail[0][logo]', '');
        }
      }
      const content = [
        {
          'text': that.header_temp_02,
          'text_color': that.colorHeaderTemplate_02
        },
        {
          'text': that.subtitle_temp_02,
          'text_color': that.colorSubtitleTemplate_02
        },
        {
          'text': that.description_temp_02,
          'text_color': that.colorDescriptionTemplate_02
        }
      ];
      datas.append('template_detail[0][id]', '' + that.id_1_templateDetail);
      datas.append('template_detail[0][type]', '' + 1);
      datas.append('template_detail[0][icon]', '');
      datas.append('template_detail[0][content]', JSON.stringify(content));
      that.feedService.updateProm(datas, that.template_id).subscribe(res => {
        that.updateLaunch(that.template_id, title, dateStart, dateEnd, timeStart, timeEnd);
      }, err => {
        console.log(err);
        swal(err.error.message, '', 'error');
      });
    }, 100);
  }

  addPromoTemplate_03(fileTop, fileBottom, title, dateStart, dateEnd, timeStart, timeEnd) {
    if (fileTop.files && fileTop.files[0]) {
      convertImageTopBase64(fileTop);
    }
    const that = this;
    const datas = new FormData();
    setTimeout(function () {
      datas.append('template_example_id', that.idTemplate);
      if (that.isChooseRadient) {
        datas.append('template_detail[0][file]', fileTop.files[0]);
        datas.append('template_detail[0][gradient]', '');
      } else {
        datas.append('template_detail[0][file]', '');
        datas.append('template_detail[0][gradient]', that.gradientChoosed);
      }
      const content = [
        {
          'text': that.header_temp_02,
          'text_color': that.colorHeaderTemplate_02
        },
        {
          'text': that.subtitle_temp_02,
          'text_color': that.colorSubtitleTemplate_02
        },
        {
          'text': that.description_temp_02,
          'text_color': that.colorDescriptionTemplate_02
        }
      ];

      datas.append('template_detail[0][type]', '' + 1);
      datas.append('template_detail[0][icon]', '');
      datas.append('template_detail[0][logo]', that.idLogoTempalte03);
      datas.append('template_detail[0][content]', JSON.stringify(content));
      // type 2
      datas.append('template_detail[1][type]', '' + 2);
      datas.append('template_detail[1][icon]', '');
      datas.append('template_detail[1][logo]', '');
      datas.append('template_detail[1][content]', '');
      datas.append('template_detail[1][file]', fileBottom.files[0]);
      datas.append('template_detail[1][gradient]', '');
      that.feedService.addPromo(datas).subscribe(res => {
        const idTemplate = res.data.id;
        console.log(res);
        that.idtemplateJustAdd = idTemplate; // id
        that.addLaunch(idTemplate, title, dateStart, dateEnd, timeStart, timeEnd);
      }, err => {
        console.log(err);
        swal(err.error.message, '', 'error');
      });
    }, 100);
  }

  updatePromoTemplate03(fileTop, fileBottom, title, dateStart, dateEnd, timeStart, timeEnd) {
    const that = this;
    if (fileTop.files && fileTop.files[0]) {
      convertImageTopBase64(fileTop);
    }
    if (fileTop.files && fileTop.files[0]) {
      convertImageTopBase64(fileTop);
    }
    const datas = new FormData();
    setTimeout(function () {
      datas.append('template_example_id', that.idTemplate);
      if (that.isChooseRadient) {
        if (fileTop.files && fileTop.files[0]) {
          datas.append('template_detail[0][file]', fileTop.files[0]);
        } else {
          datas.append('template_detail[0][file]', '');
        }
        datas.append('template_detail[0][gradient]', '');
      } else {
        datas.append('template_detail[0][file]', '');
        datas.append('template_detail[0][gradient]', that.gradientChoosed);
      }
      if (fileBottom.files && fileBottom.files[0]) {
        datas.append('template_detail[1][file]', fileBottom.files[0]);
      }
      const content = [
        {
          'text': that.header_temp_02,
          'text_color': that.colorHeaderTemplate_02
        },
        {
          'text': that.subtitle_temp_02,
          'text_color': that.colorSubtitleTemplate_02
        },
        {
          'text': that.description_temp_02,
          'text_color': that.colorDescriptionTemplate_02
        }
      ];

      datas.append('template_detail[0][id]', '' + that.id_1_templateDetail);
      datas.append('template_detail[0][type]', '' + 1);
      datas.append('template_detail[0][icon]', '');
      datas.append('template_detail[0][logo]', that.idLogoTempalte03);
      datas.append('template_detail[0][content]', JSON.stringify(content));
      // type 2
      datas.append('template_detail[1][id]', '' + that.id_2_templateDetail);
      datas.append('template_detail[1][type]', '' + 2);
      datas.append('template_detail[1][icon]', '');
      datas.append('template_detail[1][logo]', '');
      datas.append('template_detail[1][content]', '');
      datas.append('template_detail[1][gradient]', '');
      that.feedService.updateProm(datas, that.template_id).subscribe(res => {
        that.updateLaunch(that.template_id, title, dateStart, dateEnd, timeStart, timeEnd);
      }, err => {
        console.log(err);
        swal(err.error.message, '', 'error');
      });
    }, 100);
  }


  changeTileTemplate() {
    this.checkForStep2();
  }

  // check do step 2
  checkForStep2() {
    const isHeaderTitle = true; // template 1
    // if (this.headerTop !== '' && this.headerBottom !== '') {
    //   isHeaderTitle = true;
    // } else {
    //   isHeaderTitle = false;
    // }
    const isTextTemplate_2_3 = true;
    // if (this.header_temp_02 !== '' && this.subtitle_temp_02 !== '' && this.description_temp_02 !== '') {
    //   isTextTemplate_2_3 = true;
    // } else {
    //   isTextTemplate_2_3 = false;
    // }
    let isIconTemplate = false;
    if (this.idIconTop && this.idIconBottom) {
      isIconTemplate = true;
    } else {
      isIconTemplate = false;
    }
    const isGradientTop = this.gradientChoosed === '' ? false : true;
    const isGradientBottom = this.gradientChoosedBottom === '' ? false : true;
    if (this.idTemplate === 1) {
      if ((this.checkImgTop || isGradientTop) && (this.checkImgBottom || isGradientBottom)) {
        this.isDoneStep2 = true;
        $('#carouseStep3').removeClass('disableDiv');
      } else {
        this.isDoneStep2 = false;
        $('#carouseStep3').addClass('disableDiv');
      }
    } else {
      // console.log('check tempalte 02');
      // console.log(this.checkImgTop);
      // console.log(isGradientTop);
      // console.log(isTextTemplate_2_3);
      if ((this.checkImgTop || isGradientTop)) {
        this.isDoneStep2 = true;
        $('#carouseStep3').removeClass('disableDiv');
      } else {
        this.isDoneStep2 = false;
        $('#carouseStep3').addClass('disableDiv');
      }
    }

  }

  // check do step 3
  checkForStep3(title) {
    if (title.value) {
      $('.launch_promo_action').removeClass('disableDiv');
    } else {
      $('.launch_promo_action').addClass('disableDiv');
    }
  }

  changeActiveTemplate() {
    $('#btn_active_promo').toggleClass('active');
    this.isActiveSendActive = !this.isActiveSendActive;
  }

  changeActiveUpdateTemplate() {
    $('.button_active_template').toggleClass('active');
    this.isActiveTemplateUpdate = !this.isActiveTemplateUpdate;
    this.feedService.sendActiveTemplatePromo({
      'template_id': this.template_id,
      'status': this.isActiveTemplateUpdate
    }).subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    });

  }

  // setCanvas
  setCanvas() {
    setCanVasToLaunch(this.colorTopHeader_top);
  }

  addLaunch(idTemplate, title, dateStart, dateEnd, timeStart, timeEnd) {
    let file;
    if (this.isChooseRadient) {
      if (base64StringTopInJs) {
        file = base64StringTopInJs;
      }
    } else {
      file = gradientImageTop;
    }
    const data = {
      'template_id': idTemplate,
      'title': title.value,
      'file': file,
      'date_start': dateStart.value,
      'date_end': dateEnd.value,
      'time_start': timeStart.value,
      'time_end': timeEnd.value
    };
    this.feedService.launchPromo(data).subscribe(res => {
      if (this.isActiveSendActive) {
        this.sendActivePromo();
      } else {
      }

      this.getFeedsMonth();
      this.getAllFeed();
      swal('Success!', '', 'success');
      // reset field
      this.resetStep2();
      this.resetStep03(title, dateStart, dateEnd, timeStart, timeEnd);
      this.titlePromo = 'Select Template';
      $('#closeAddPromo').click();
    }, err => {
      this.getFeedsMonth();
      this.getAllFeed();
      swal(err.error.message, '', 'error');
    });
  }

  updateLaunch(idTemplate, title, dateStart, dateEnd, timeStart, timeEnd) {
    let file;
    if (base64StringTopInJs) {
      file = base64StringTopInJs;
    } else {
      file = gradientImageTop;
    }
    const data = {
      'template_id': idTemplate,
      'title': title.value,
      'file': file,
      'date_start': dateStart.value,
      'date_end': dateEnd.value,
      'time_start': timeStart.value,
      'time_end': timeEnd.value
    };
    this.feedService.updatLaunchPromo(data).subscribe(res => {
      if (this.isActiveSendActive) {
        console.log(1);
        this.sendActivePromo();
      } else {
        console.log(0);
      }

      this.getFeedsMonth();
      this.getAllFeed();
      swal('Success!', '', 'success');
      // reset field
      this.resetStep2();
      this.resetStep03(title, dateStart, dateEnd, timeStart, timeEnd);
      this.titlePromo = 'Select Template';
      $('#closeAddPromo').click();
    }, err => {
      this.getFeedsMonth();
      this.getAllFeed();
      swal(err.error.message, '', 'error');
    });
  }

  // Reset field Promo
  resetStep2() {
    this.isCheckLogo = true;

    this.isImageTop = true;
    this.isImageBottom = true;
    this.isImagePromotemplate02 = true;
    this.isImageLogotemplate02 = true;
    this.isImagePromotemplate03 = true;
    this.isVideoPromotemplate03 = true;
    // template 01
    base64StringTopInJs = '';
    base64StringBottomInJs = '';
    this.colorTopHeader_top = '#000000';
    $('#filePromo').val('');
    $('#filePromo_bottom').val('');
    this.headerTop = 'Header Display Here (37 characters)';
    this.headerBottom = 'Header Display Here (37 characters)';
    this.idIconTop = null;
    this.idIconBottom = null;
    $('.arrow_item').removeClass('active');
    $('.img_template_top').attr('src', '');
    $('.img_template_top').attr('background', 'transparent');
    $('.img_template_bottom').attr('src', '');
    $('.img_template_bottom').attr('background', 'transparent');
    $('.icon_top_template').attr('background', '#ca6565');
    $('.icon_bottom_template').attr('background', '#ca6565');
    $('.gradient_template_01_top ').attr('src', '');
    $('.gradient_template_01_bottom ').attr('src', '');
    $('#check_image_temp_01_top').click();
    $('#check_image_temp_01_bottom').click();
    this.colorTopHeader_top = '#000000';
    this.colorTopHeader_bottom = '#000000';
    $('.text_color_white_promo').click();
    $('.custom-file-label').text('Upload image');
    $('#lable-file-video-promo_03').text('Upload video');
    $('.icon_template').attr('src', '');
    // template 02
    this.header_temp_02 = 'KPMG Welcomes';
    this.subtitle_temp_02 = 'MD of ABC Health';
    this.description_temp_02 = 'Dr. John Loh';
    this.colorHeaderTemplate_02 = '#000000';
    this.colorSubtitleTemplate_02 = '#000000';
    this.colorDescriptionTemplate_02 = '#000000';
    $('#fileLogoPromo_02').val('');
    $('#filePromo_02').val('');
    $('.img_template_02').attr('src', '');
    $('.img_template_02').attr('background', 'transparent');
    $('.img_logo_template_02').attr('src', '');
    $('.gradient_template_02 ').attr('src', '');
    $('.img_logo_template_02').attr('background', 'transparent');
    $('#check_image_temp_02').click();
    $('#carouseStep3').addClass('disableDiv');
    gradientImageTop = '';


    // reset gradient
    this.isChooseRadient = 'disable';
    this.isChooseImage = '';
    this.isChooseRadientBottom = 'disable';
    this.isChooseImageBottom = '';
    this.gradientChoosed = '';
    this.gradientChoosedBottom = '';
    // reset tempalte 03
    $('.img_template_03  ').attr('src', '');
    $('.gradient_template_03 ').attr('src', '');
    $('.video_template03').attr('src', '');
    $('.gradient_template_01_top').css('background', 'transparent');
    $('.gradient_template_01_bottom').css('background', 'transparent');
    $('.gradient_template_02 ').css('background', 'transparent');
    $('.gradient_template_03 ').css('background', 'transparent');
    $('#logo_template_03_1').click();
    $('#fileVideoPromo_03').val('');

  }

  resetStep03(title, dateStart, dateEnd, timeStart, timeEnd) {
    title.value = '';
    dateStart.value = '';
    dateEnd.value = '';
    timeStart.value = '';
    timeEnd.value = '';
    $('#step1').click();
    $('.content-item_01').removeClass('active');
  }

  // send active promo
  sendActivePromo() {
    this.feedService.sendActiveTemplatePromo({'template_id': this.idtemplateJustAdd}).subscribe(res => {
      swal('Active Success!', '', 'success');
    }, err => {
      swal('Can not Active this template!', '', 'error');
    });
  }

  getPromoById(id) {
    this.feedService.getTemplateById(id).subscribe(res => {
      console.log(res.data);
      // check type template
      this.checkTemplateId(res.data.template_example_id);
      this.resetFieldUpdatePromo(res.data);
    }, err => {
      console.log(err);
    });
  }

  resetFieldUpdatePromo(data) {
    const that = this;
    $('#step2').click();
    // set status
    if (data.status === 1) {
      $('#button_active_template').addClass('active');
      this.isActiveTemplateUpdate = true;
    } else {
      $('#button_active_template').removeClass( 'active');
      this.isActiveTemplateUpdate = false;
    }
    $('#carouseStep3').removeClass('disableDiv');
    // set field status
    this.idTemplate = data.template_example_id;
    if (this.idTemplate === 1) {
      this.resetUpdateTempalte01(data);
    } else {
      if (this.idTemplate === 2) {
        this.resetUpdateTempalte02(data);
      } else {
        this.resetUpdateTempalte03(data);
      }
    }

  }

  resetUpdateTempalte01(data) {
    const that = this;
    this.getIconTop();
    this.getIconBottom();
    data.template_detail.forEach(function (value) {
      if (value.type === 1) {
        that.id_1_templateDetail = value.id;
        that.colorTopHeader_top = value.content[0].text_color;
        that.headerTop = value.content[0].text;
        // process gradient
        if (value.gradient) {
          const idGradient = '#gradient_temp01_top_' + value.gradient;
          $('.gradient_template_01_top ').css('opacity', '1');
          setTimeout(function () {
            $(idGradient).click();
            $(idGradient).addClass('active');
          }, 600);
          that.isChooseRadient = '';
          that.isChooseImage = 'disable';
          $('#check_image_temp_01_top').prop('checked', false);
          $('#check_gradient_temp_01_top').prop('checked', true);
        } else {
          $('.img_template_top').css('display', 'inline-block');
        }
        const id_1 = '#icon_' + value.icon;

        setTimeout(function () {
          const urlTop = $(id_1 + ' img').attr('src');
          that.iconTopUrl = urlTop;
          $('.iconUrl_top_template ').attr('src', urlTop);
          $(id_1).click();
        }, 1000);
        $('.img_template_top').attr('src', value.file);
        const colorClick = value.content[0] + '_1';
        $(value.content[0].text_color + '_1').click();

      } else {
        that.id_2_templateDetail = value.id;
        that.colorTopHeader_bottom = value.content[0].text_color;
        that.headerBottom = value.content[0].text;
        // process gradient
        if (value.gradient) {
          const idGradient = '#gradient_temp01_bottom_' + value.gradient;
          $('.gradient_template_01_bottom ').css('opacity', '1');
          setTimeout(function () {
            $(idGradient).click();
            $(idGradient).addClass('active');
          }, 600);
          that.isChooseRadientBottom = '';
          that.isChooseImageBottom = 'disable';
          $('#check_image_temp_01_bottom').prop('checked', false);
          $('#check_gradient_temp_01_bottom').prop('checked', true);
        }
        const id_2 = '#icon_' + value.icon;
        setTimeout(function () {
          const urlTop = $(id_2 + ' img').attr('src');
          that.iconBottomUrl = urlTop;
          $('.iconUrl_bottom_template ').attr('src', urlTop);
          $(id_2).click();
        }, 1000);
        $('.img_template_bottom').attr('src', value.file);
        $(value.content[0].text_color + '_2').click();
      }
    });
    setTimeout(function () {
      $('#topLayout').click();
    }, 1001);
    this.checkImgBottom = true;
    this.checkImgTop = true;
    $('#carouseStep3').removeClass('disableDiv');
    $('.launch_promo_action  ').removeClass('disableDiv');
    // DONE STEP 3
    $('#title_promo').val(data.title);
    $('#start_day_promo').val(data.date_start);
    $('#end_day_promo').val(data.date_end);
    $('#time_start_promo').val(data.time_start);
    $('#time_end_promo').val(data.time_end);
    $('#launch_promo').removeClass('disableDiv');
  }

  resetUpdateTempalte02(data) {
    const that = this;
    data.template_detail.forEach(function (value) {
      if (value.type === 1) {
        that.id_1_templateDetail = value.id;
        that.colorHeaderTemplate_02 = value.content[0].text_color;
        that.colorClick(that.colorHeaderTemplate_02, '_temp_02_header');
        that.header_temp_02 = value.content[0].text;
        that.colorSubtitleTemplate_02 = value.content[1].text_color;
        that.colorClick(that.colorSubtitleTemplate_02, '_temp_02_subtitle');
        that.subtitle_temp_02 = value.content[1].text;
        that.colorDescriptionTemplate_02 = value.content[2].text_color;
        that.colorClick(that.colorDescriptionTemplate_02, '_temp_02_description');
        that.description_temp_02 = value.content[2].text;
        // if not use gradient
        if (!value.gradient) {
          $('.img_template_02 ').attr('src', value.file);
          $('.img_template_02 ').css('display', 'inline-block');

          that.isChooseRadient = 'disable';
          that.isChooseImage = '';
        } else {
          const idGradient = '#gradient_temp02_' + value.gradient;
          setTimeout(function () {
            $(idGradient).click();
            $(idGradient).addClass('active');
          }, 600);
          that.isChooseRadient = '';
          that.isChooseImage = 'disable';
          $('#check_image_temp_02').prop('checked', false);
          $('.gradient_template_02').css('opacity', 1);
          $('#check_gradient_temp_02').prop('checked', true);

        }
        if (value.logo) {
          $('.img_logo_template_02 ').attr('src', value.logo);
          $('.logo_bottom_template_02').removeClass('d-none');
        } else {
          $('.img_logo_template_02 ').attr('src', '');
          $('.logo_bottom_template_02').addClass('d-none');
        }
        $(value.text_color + '_1').click();
      } else {
      }
    });
    setTimeout(function () {
      $('#topLayout').click();
    }, 1001);
    this.checkImgBottom = true;
    this.checkImgTop = true;
    $('#carouseStep3').removeClass('disableDiv');
    $('.launch_promo_action  ').removeClass('disableDiv');

    // DONE STEP 3
    $('#title_promo').val(data.title);
    $('#start_day_promo').val(data.date_start);
    $('#end_day_promo').val(data.date_end);
    $('#time_start_promo').val(data.time_start);
    $('#time_end_promo').val(data.time_end);
    $('#launch_promo').removeClass('disableDiv');
  }

  resetUpdateTempalte03(data) {
    const that = this;
    $('#top_template_03').click();
    data.template_detail.forEach(function (value) {
      if (value.type === 1) {
        that.id_1_templateDetail = value.id;
        that.colorHeaderTemplate_02 = value.content[0].text_color;
        that.colorClick(that.colorHeaderTemplate_02, '_temp_03_header');
        that.header_temp_02 = value.content[0].text;
        that.colorSubtitleTemplate_02 = value.content[1].text_color;
        that.colorClick(that.colorSubtitleTemplate_02, '_temp_03_subtitle');
        that.subtitle_temp_02 = value.content[1].text;
        that.colorDescriptionTemplate_02 = value.content[2].text_color;
        that.colorClick(that.colorDescriptionTemplate_02, '_temp_03_description');
        that.description_temp_02 = value.content[2].text;
        // if not use gradient
        if (!value.gradient) {
          $('.img_template_03').attr('src', value.file);
          $('.img_template_03').css('display', 'inline-block');
        } else {
          $('.img_template_03').attr('src', '');
          $('.img_template_03').css('display', 'none');
          // set gradient for template
          const idGradient = '#gradient_temp03_' + value.gradient;
          setTimeout(function () {
            $(idGradient).click();
            $(idGradient).addClass('active');
          }, 600);
          that.isChooseRadient = '';
          that.isChooseImage = 'disable';
          $('#check_image_temp_03').prop('checked', false);
          $('#check_gradient_temp_03').prop('checked', true);
        }
        if (value.logo) {
          const id = '#logo_template_03_' + value.logo;
          $(id).click();
          $(id).addClass('active');
        } else {
          // code for not have logo
        }
        $(value.text_color + '_1').click();
      } else {
        that.id_2_templateDetail = value.id;
        $('.video_template03').attr('src', value.file);
      }
    });
    setTimeout(function () {
      $('#topLayout').click();
    }, 1001);
    this.checkImgBottom = true;
    this.checkImgTop = true;
    $('#carouseStep3').removeClass('disableDiv');
    $('.launch_promo_action  ').removeClass('disableDiv');
    // DONE STEP 3
    $('#title_promo').val(data.title);
    $('#start_day_promo').val(data.date_start);
    $('#end_day_promo').val(data.date_end);
    $('#time_start_promo').val(data.time_start);
    $('#time_end_promo').val(data.time_end);
    $('#launch_promo').removeClass('disableDiv');
  }

  colorClick(idPre, obj) {
    const headerClick = idPre + obj;
    $(headerClick).click();
  }

  // End Api for promo

  eventAction() {
    let event;
    this.isActiveEvent = !this.isActiveEvent;
    if (this.isActiveEvent) {
      $('#button_event').addClass('event_active');
    } else {
      $('#button_event').removeClass('event_active');
    }
    if (this.isActiveEvent) {
      event = {'event': true};

    } else {
      event = {'event': false};
    }
    this.feedService.eventSocket(event);
  }

  setAddRuleFeed(file, timeStart, timeEnd) {
    this.changeOptionTrue();
    this.isAddRuleAction = true;
    this.titleRuleAction = 'Add Rule Feed Information';
    // Reset field
    this.feedInfoRule = new RuleFeedInformation();
    $('#blah0_feedInfoRule').val('');
    timeStart.value = '';
    timeEnd.value = '';
    this.inputFeedRule = new FormData();
    $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
    $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
    $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
  }

  // Rule feed information
  addNewRule(file, timeStart, timeEnd) {
    this.feedInfoRule.feed_id = this.feedParentId;

    if (this.isAddImage) {
      if (!file.files[0]) {
        this.inputFeedRule.append('file', null);
      } else {
        this.inputFeedRule.append('file', file.files[0]);
      }

    } else {
      this.inputFeedRule.append('file', this.feedInfoRule.file);
    }

    this.inputFeedRule.append('feed_id', this.feedInfoRule.feed_id);
    this.inputFeedRule.append('element', this.feedInfoRule.element);
    this.inputFeedRule.append('prerequisite', this.feedInfoRule.prerequisite);
    this.inputFeedRule.append('value', this.feedInfoRule.value);
    this.inputFeedRule.append('type', this.feedInfoRule.type);
    this.inputFeedRule.append('title', this.feedInfoRule.title);
    this.inputFeedRule.append('content', this.feedInfoRule.content);
    this.inputFeedRule.append('time_start', timeStart.value);
    this.inputFeedRule.append('time_end', timeEnd.value);
    this.feedService.addRuleFeedInformation(this.inputFeedRule).subscribe(res => {
      swal('Add Rule  Success!', '', 'success');
      this.getRulesFeed(this.feedParentId);
      // Reset field
      this.feedInfoRule = new RuleFeedInformation();
      $('#blah0_feedInfoRule').val('');
      timeStart.value = '';
      timeEnd.value = '';
      this.inputFeedRule = new FormData();
      $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
      $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
      $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
    }, err => {
      swal('Add Rule Failure!', '', 'error');
    });
  }

  // Rule feed information
  updateRuleFeed(file, timeStart, timeEnd) {
    this.feedInfoRule.feed_id = this.feedParentId;

    if (this.isAddImage) {
      if (!file.files[0]) {
        this.inputFeedRule.append('file', null);
      } else {
        this.inputFeedRule.append('file', file.files[0]);
      }

    } else {
      this.inputFeedRule.append('file', this.feedInfoRule.file);
    }

    this.inputFeedRule.append('feed_id', this.feedInfoRule.feed_id);
    this.inputFeedRule.append('element', this.feedInfoRule.element);
    this.inputFeedRule.append('prerequisite', this.feedInfoRule.prerequisite);
    this.inputFeedRule.append('value', this.feedInfoRule.value);
    this.inputFeedRule.append('type', this.feedInfoRule.type);
    this.inputFeedRule.append('title', this.feedInfoRule.title);
    this.inputFeedRule.append('content', this.feedInfoRule.content);
    this.inputFeedRule.append('time_start', timeStart.value);
    this.inputFeedRule.append('time_end', timeEnd.value);

    this.feedService.updateRuleFeed(this.ruleId, this.inputFeedRule).subscribe(res => {
      swal('Update Rule  Success!', '', 'success');
      this.getRulesFeed(this.feedParentId);
      // Reset field
      this.feedInfoRule = new RuleFeedInformation();
      $('#blah0_feedInfoRule').val('');
      timeStart.value = '';
      timeEnd.value = '';
      this.inputFeedRule = new FormData();
      $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
      $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
      $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
    }, err => {
      swal('Update Rule Failure!', '', 'error');
    });
  }

  deleteFeedRule(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Rule!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.feedService.deleteFeedRule(id).subscribe(res => {
            swal('Delete Success!', '', 'success');
            this.getRulesFeed(this.feedParentId);
          }, error1 => {
            console.log(error1);
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });

  }

  getDetailRule(id) {
    this.ruleId = id;
    this.isAddRuleAction = false;
    this.titleRuleAction = 'Update Rule Feed Information';
    this.feedService.getDetailRule(id).subscribe(res => {
      this.setUpdateRuleInfomation(res.data);
    }, err => {
      console.log(err);
    });
  }

  setUpdateRuleInfomation(data) {
    $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
    $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
    $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
    this.feedInfoRule.file = '';
    this.feedInfoRule.type = data.type;
    this.feedInfoRule.title = data.title;
    this.feedInfoRule.content = data.content;
    this.feedInfoRule.element = data.element;
    this.feedInfoRule.prerequisite = data.prerequisite;
    this.feedInfoRule.value = data.value;
    this.feedInfoRule.time_start = data.time_start;
    this.feedInfoRule.time_end = data.time_end;
    this.isAddImage = true;
    if (data.file.icon) {
      this.showIcon = 'd-block';
      this.showImage = 'd-none';
      this.isAddImage = false;
      this.feedInfoRule.file = data.file.icon;
    } else {
      if (data.file.image) {
        this.isAddImage = true;
        this.showImage = 'd-block';
        this.showIcon = 'd-none';
        $('.blah0_feedInfoRule_plus_img').css('display', 'none');
        $('.blah0_feedInfoRule_add_img').css('display', 'none');
        $('#blah0_feedInfoRule_img').attr('src', data.file.image);
      } else {
        this.isAddImage = true;
        $('.blah0_feedInfoRule_plus_img').css('display', 'none');
        $('.blah0_feedInfoRule_add_img').css('display', 'none');
        $('#blah0_feedInfoRule_img').attr('src', data.file);
      }
    }
  }


  // get condition and operator
  getConditions() {
    this.getOperator();
    this.ruleService.getCondition().subscribe(res => {
      this.condition = res.data;
    }, err => {
      console.log(err);
    });
  }


  getOperator() {
    this.ruleService.getOperator().subscribe(res => {
      this.operatorr = res.data;
    }, error => {
      console.log(error);
    });
  }

  getIdEventMonth(id) {
    this.idUpdate = id;
  }

  getAllFeed() {
    const that = this;
    // get All Data
    this.subcription1 = this.feedService.getAllFeed().subscribe(res => {
      this.isLoadingCalendar = false;
      $('#calendar').fullCalendar('destroy');
      feedFunction(res.data);
      show_icon(res.data);
    }, error1 => {
      console.log(error1);
    });
  }

  getFeedsMonth() {
    const datas = [];
    this.subcription2 = this.feedService.getFeedsWithMonth().subscribe(res => {
      $.each(res.data, function (index, item) {
        datas.push({
          title: index,
          event: item
        });
      });
      this.dataMonth = datas;
    }, error1 => {
      console.log(error1);
    });
  }

// Add Feed
  addFeed(file, dateStart, dateEnd, timeStart, timeEnd, favorite) {
    this.feed.date_start = dateStart.value;
    this.feed.date_end = dateEnd.value;
    this.feed.time_start = timeStart.value;
    this.feed.time_end = timeEnd.value;
    this.feed.favorite = $('#favorite_feed').val();
    this.feed.feed_emoji = $('#content_chat_addFeed').html();
    this.feed.description = this.convertString('#content_chat_addFeed');
    // console.log('convert: ');
    // console.log(this.feed.description);
    // console.log('html');
    // console.log(this.feed.feed_emoji);
    // console.log(file.files[0]);
    // console.log(this.feed.date_start);
    // console.log(this.feed.date_end);
    // console.log(this.feed.time_start);
    // console.log(this.feed.time_end);
    // console.log(this.feed.title);
    // console.log(this.feed.time_repeat);
    // console.log(this.feed.favorite);
    // console.log(this.feed.feed_emoji);
    if (!file.files[0]) {
      this.input.append('file', null);
    } else {
      this.input.append('file', file.files[0]);
    }
    this.input.append('title', this.feed.title);
    this.input.append('description', this.feed.description);
    this.input.append('date_start', this.feed.date_start);
    this.input.append('date_end', this.feed.date_end);
    this.input.append('time_start', this.feed.time_start);
    this.input.append('time_end', this.feed.time_end);
    this.input.append('favorite', this.feed.favorite);
    this.input.append('time_repeat', this.feed.time_repeat);
    this.input.append('feed_emoji', this.feed.feed_emoji);

    this.feedService.addFeed(this.input).subscribe(res => {

      this.getFeedsMonth();
      this.getAllFeed();
      $('.close').click();
      swal('Add Success!', '', 'success');
      this.input = new FormData();
      this.feed = new Feed();
      dateStart.value = '';
      dateEnd.value = '';
      timeStart.value = '';
      timeEnd.value = '';
      $('#margin').val('');
      $('#favorite_feed').val('');
      $('#favorite_feed').select2();
      $('.blah0_addFeed').attr('src', '').removeAttr('style');
      $('.blah0_addFeed_plus_img').css('display', 'inline-block');
      $('#btn_addimg').css('display', 'block');
      $('#content_chat_addFeed').html('');
      $('#add_img').css('display', 'inline-block');
      css();

    }, error => {
      this.getFeedsMonth();
      this.getAllFeed();
      console.log(error);
      swal('All field must have value!', '', 'error');

    });

  }

  // convert chat to back end
  convertString(obj) {
    const chat = $(obj).html();
    let result = '';
    result = chat.replace(/<p>/g, '').replace(/<[/]p>/g, '')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiaddFeed/g, 'sprite index=')
      .replace(/">/g, '>').replace(/font color/g, 'color').replace(/[/]font/g, '/color').replace(/&nbsp;/g, ' ').replace(/<div>|<[/]div>/g, '')
      .replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
    return result;
  }

  convertStringUpdate(obj) {
    const chat = $(obj).html();
    let result = '';
    result = chat.replace(/<p>/g, '').replace(/<[/]p>/g, '')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiupdateFeed/g, 'sprite index=')
      .replace(/">/g, '>').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&').replace(/<div>|<[/]div>/g, '')
      .replace(/font color/g, 'color').replace(/[/]font/g, '/color').replace(/&nbsp;/g, ' ')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiaddFeed/g, 'sprite index=');
    return result;
  }

  // check image width height
  checkImagePromo(isTop, file) {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      if (isTop === 1) {
        // check image change
        that.isImageTop = isImagePromo;
        if (file.value && isImagePromo) {
          that.checkImgTop = true;
          that.checkForStep2();
        } else {
          that.checkImgTop = false;
          that.checkForStep2();
        }
      } else {
        that.isImageBottom = isImagePromo;
        // check image change
        if (file.value && isImagePromo) {
          that.checkImgBottom = true;
          that.checkForStep2();
        } else {
          that.checkImgBottom = false;
          that.checkForStep2();
        }
      }
    }, 20);
  }

  // check image feed
  checkImagePromoTemplate_02(file) {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isImagePromotemplate02 = isImagePromTemplate_02;
      if (file.value && isImagePromTemplate_02) {
        that.checkImgTop = true;
        that.checkForStep2();
      } else {
        that.checkImgTop = false;
        that.checkForStep2();
      }

    }, 20);
  }

  // check image feed
  checkImageLogoTemplate_02() {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isImageLogotemplate02 = isImageLogoPromTemplate_02;
    }, 20);
  }

  // template 03
  checkImagePromoTemplate_03(file) {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isImagePromotemplate03 = isImagePromTemplate_03;
      if (file.value && isImagePromTemplate_03) {
        that.checkImgTop = true;
        that.checkForStep2();
      } else {
        that.checkImgTop = false;
        that.checkForStep2();
      }
    }, 20);
  }

  checkVideoPromoTemplate_03() {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isVideoPromotemplate03 = isVideoPromTemplate_03;
    }, 20);
  }

// check image feed
  checkImageFeed() {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isFeedImage = isImageFeed;
    }, 20);
  }

  // check image Feed Info width height
  checkImageFeedInfo() {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isFeedInfoImage = isImageFeedInfo;
    }, 20);
  }

  getRecurrence(value) {
    this.feed.time_repeat = value;
    this.updateFeed.time_repeat = value;
  }

  deleteEvent(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Event!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.feedService.deleteEvent(id).subscribe(res => {
            this.getFeedsMonth();
            $('#calendar').fullCalendar('destroy');
            this.getAllFeed();
            monthClick();
            swal('Delete Success!', '', 'success');
          }, error1 => {
            swal('Can not delete event!', '', 'error');
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

// Update Feed
  getFeedById(id) {
    this.feedService.getFeedById(id).subscribe(res => {
      this.feedById = res.data;
      this.feed.time_repeat = this.feedById.time_repeat;
      this.setDataFieldInputUpdate();
    }, err => {
      console.log(err);
    });
  }

  // flow data to modal box update feed
  setDataFieldInputUpdate() {
    css();
    const recurrent = this.feedById.time_repeat;
    const objClick = '.recurrence-' + recurrent;
    $(objClick).click();
    $('.blah0_updateFeed_plus_img').css('display', 'none');
    $('.blah0_updateFeed_add_img').css('display', 'none');
    $('#img_file').attr('src', this.feedById.file);
    $('#title_update').val(this.feedById.title);
    $('#content_chat_updateFeed').html(this.feedById.feed_emoji);
    this.updateFeed.date_start = this.feedById.date_start;
    this.updateFeed.date_end = this.feedById.date_end;
    this.updateFeed.time_start = this.feedById.time_start;
    this.updateFeed.time_end = this.feedById.time_end;
    setTag(this.idUpdate, this.IP);
  }

  // update Feed Action
  updateFeedAction(file, date_start, date_end, time_start, time_end) {
    this.updateFeed.feed_emoji = $('#content_chat_updateFeed').html();
    this.updateFeed.description = this.convertStringUpdate('#content_chat_updateFeed');
    if (file.files[0]) {
      this.updateFeed.file = file.files[0];
    }
    if (!this.updateFeed.title) {
      this.updateFeed.title = this.feedById.title;
    }
    if (!this.updateFeed.description) {
      this.updateFeed.description = this.feedById.description;
    }
    this.updateFeed.favorite = $('#favorite_feedUpdate').val().toString();

    if (!date_start) {
      this.inputUpdate.append('date_start', null);
    } else {
      this.inputUpdate.append('date_start', date_start.value);
    }

    if (!date_end) {
      this.inputUpdate.append('date_end', null);
    } else {
      this.inputUpdate.append('date_end', date_end.value);
    }

    if (!time_start) {
      this.inputUpdate.append('time_start', null);
    } else {
      this.inputUpdate.append('time_start', time_start.value);

    }
    if (!time_end) {
      this.inputUpdate.append('time_end', null);
    } else {
      this.inputUpdate.append('time_end', time_end.value);
    }

    // console.log('id: ');
    // console.log(this.idUpdate);
    // console.log('file : ');
    // console.log(this.updateFeed.file);
    // console.log('title : ');
    // console.log(this.updateFeed.title);
    // console.log('descritiom : ');
    // console.log(this.updateFeed.description);
    // console.log('emoiji : ');
    // console.log(this.updateFeed.feed_emoji);
    // console.log('date start : ');
    // console.log(date_start.value);
    // console.log('date end : ');
    // console.log(date_end.value);
    // console.log('time start : ');
    // console.log(time_start.value);
    // console.log('date end : ');
    // console.log(time_end.value);
    // console.log('Time Repeat : ');
    // console.log(this.updateFeed.time_repeat);
    // console.log('Preference Tags : ');
    // console.log(this.updateFeed.favorite);

    this.inputUpdate.append('title', this.updateFeed.title);
    this.inputUpdate.append('description', this.updateFeed.description);
    this.inputUpdate.append('favorite', this.updateFeed.favorite);
    this.inputUpdate.append('file', this.updateFeed.file);
    this.inputUpdate.append('time_repeat', this.updateFeed.time_repeat);
    this.inputUpdate.append('feed_emoji', this.updateFeed.feed_emoji);

    this.feedService.updateFeed(this.idUpdate, this.inputUpdate).subscribe(res => {
      $('.close').click();
      swal('Update Success!', '', 'success');
      $('#calendar').fullCalendar('destroy');
      this.getAllFeed();
      this.getFeedsMonth();
      $('.fc-view-container').css('display', 'block');
      $('.card_list').css('display', 'none');
    }, error => {
      swal('Update Failure!!', '', 'error');
    });
    // reset fields
    $('#margin_update').val('');
    this.inputUpdate = new FormData();
    this.updateFeed = new Feed();
    date_start.value = '';
    date_end.value = '';

  }

  checkDateEvent(date_start, date_end) {
    if (!date_start && !date_end) {
      return true;
    }
    if (!date_start || !date_end) {
      return false;
    }
    if (date_start > date_end) {
      return false;
    } else {
      return true;
    }
  }

  checkDateTemplatePromo(date_start, date_end, time_start, time_end) {
    if (!date_start && !date_end && !time_end && !time_end) {
      return false;
    }
    if (!date_start || !date_end || !time_start || !time_end) {
      return false;

    }
    if (date_start > date_end) {
      return false;
    } else {
      if (date_start === date_end) {
        if (time_start >= time_end) {
          return false;
        } else {
          this.isDoneStep3 = true;
          return true;
        }
      } else {
        this.isDoneStep3 = true;
        return true;
      }
    }
  }

  ngOnDestroy() {
    this.subcription1.unsubscribe();
    this.subcription2.unsubscribe();
  }

  // Add Feed Information
  addFeedInformation(dateStart, dateEnd, file) {
    this.inputFeedInfo.append('name', this.feedInfor.name);
    this.inputFeedInfo.append('url', this.feedInfor.url);
    this.inputFeedInfo.append('file', file.files[0]);
    this.inputFeedInfo.append('description', this.feedInfor.description);
    this.inputFeedInfo.append('time_start', dateStart.value);
    this.inputFeedInfo.append('time_end', dateEnd.value);
    this.inputFeedInfo.append('time_repeat', this.feedInfor.time_repeat);
    this.feedService.addFeedInformation(this.inputFeedInfo).subscribe(res => {

      this.getFeedInfomation();
      $('#close_addFeedInfo').click();
      // reset field
      dateStart.value = '';
      dateEnd.value = '';
      $('.blah0_addFeedInfo').attr('src', '').removeAttr('style');
      $('#plus_imgex').css('display', 'inline-block');
      $('#btn_addimg_feedInfo').css('display', 'block');
      this.feedInfor = new FeedInfor();
      swal('Add Success!', '', 'success');
    }, err => {
      swal('All field must have value!!', '', 'error');
    });
  }


  // Get Feed Information
  getFeedInfomation() {
    this.feedService.getAllFeedInformation().subscribe(res => {
      this.destroyDataTable('#table_feedInfo');
      this.allFeedInfo = res.data;
      // config dataTables
      this.dataTablesConfig('#table_feedInfo', '#navigation_feedInfo');
    }, err => {
      console.log(err);
    });
  }

  deleteFeedInfo(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Feed Information!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.feedService.deleteFeedInfo(id).subscribe(res => {
            this.getFeedInfomation();
            swal('Delete Success!', '', 'success');
          }, error1 => {
            console.log(error1);
            swal('Delete Failure!', '', 'error');
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

  getFeedInforById(id) {
    this.idFeedInfo = id;
    this.feedService.getFeedInfoById(id).subscribe(res => {
      this.oneFeedInfo = res.data;
      this.setFeedInforUpdate();
    }, err => {
      console.log(err);
    });
  }

  setFeedInforUpdate() {
    $('.blah0_updateFeedInfo_plus_img').css('display', 'none');
    $('#add_img_feed_info_update').css('display', 'none');
    $('#img_file_info_update').attr('src', this.oneFeedInfo.file);
    $('#title_update').val(this.oneFeedInfo.title);
    $('#description_update').val(this.oneFeedInfo.description);
    this.feedInforUpdate.name = this.oneFeedInfo.name;
    this.feedInforUpdate.url = this.oneFeedInfo.url;
    this.feedInforUpdate.description = this.oneFeedInfo.description;
    this.feedInforUpdate.time_repeat = this.oneFeedInfo.time_repeat;
    this.feedInforUpdate.date_start = this.oneFeedInfo.time_start;
    this.feedInforUpdate.date_end = this.oneFeedInfo.time_end;
  }

// update Feed Information
  updateFeedInfo(dateStart, dateEnd, file) {
    if (file.files[0]) {
      this.inputFeedInfoUpdate.append('file', file.files[0]);
    } else {
      this.inputFeedInfoUpdate.append('file', null);
    }
    this.inputFeedInfoUpdate.append('name', this.feedInforUpdate.name);
    this.inputFeedInfoUpdate.append('url', this.feedInforUpdate.url);
    this.inputFeedInfoUpdate.append('description', this.feedInforUpdate.description);
    this.inputFeedInfoUpdate.append('time_start', dateStart.value);
    this.inputFeedInfoUpdate.append('time_end', dateEnd.value);
    this.inputFeedInfoUpdate.append('time_repeat', this.feedInforUpdate.time_repeat);

    this.feedService.updateFeedInfoById(this.idFeedInfo, this.inputFeedInfoUpdate).subscribe(res => {
      this.getFeedInfomation();
      $('#close_addFeedInfoUpdate').click();
      swal('Update Success!', '', 'success');
      this.inputFeedInfoUpdate = new FormData();
      $('.blah0').attr('src', '').removeAttr('style');
      $('#add_img_feed_info_update').css('display', 'block');
      $('#plus_imgex').css('display', 'inline-block');
      $('#file_infor_feed_update').val('');
    }, err => {
      swal('All field must have value!!', '', 'error');
    });
  }

  // Rules Feed
  getRulesFeed(id) {
    this.feedParentId = id;
    this.feedService.getRuleFeedInfo(id).subscribe(res => {
      this.feedRules = res.data;
      this.destroyDataTable('#table-rules-feedInfo');
      this.chRef.detectChanges();
      $('#table-rules-feedInfo').DataTable({
        'pageLength': 100000,
      });
    }, err => {
      console.log(err);
    });
  }

// change Option
  changeOptionTrue() {
    this.isAddImage = true;
    this.showImage = 'd-block';
    this.showIcon = 'd-none';
    this.feedInfoRule.type = '5';
  }

  changeOptionFalse() {
    this.isAddImage = false;
    this.showImage = 'd-none';
    this.showIcon = 'd-block';
    this.feedInfoRule.type = '3';
  }

  // dataTable function
  dataTablesConfig(objTable, objPagi) {
    // You'll have to wait that changeDetection occurs and projects data into
    // the HTML template, you can ask Angular to that for you ;-)
    this.chRef.detectChanges();
    $(objTable).DataTable({
      'pageLength': 10
    });
    this.setPagiInfo(objTable, objPagi);
    this.actionPagiPage(objTable, objPagi);
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }

  setPagiInfo(objTable, objPagi) {
    const table = $(objTable).DataTable();
    if (table && table.page.info()) {
      const currentPage = table.page.info().page + 1;
      const totalPage = table.page.info().pages;
      const pagi = 'Page ' + currentPage + ' of ' + totalPage;
      $(objPagi + ' .pagin').html(pagi);
    } else {
      $(objPagi + ' .pagin').html('Page 1 of 1');
    }
  }

  actionPagiPage(objTable, objPagi) {
    const that = this;
    $(objTable + ' th').on('click', function () {
      that.setPagiInfo(objTable, objPagi);
    });
    // action pagination page
    const table = $(objTable).DataTable();
    $(objPagi + ' .ic_next').on('click', function () {
      table.page('next').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
    $(objPagi + ' .ic_prev').on('click', function () {
      table.page('previous').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
  }
}
